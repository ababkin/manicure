﻿using UnityEngine;
using System.Collections;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using System;

public class AppodealController : MonoBehaviour, IInterstitialAdListener, INonSkippableVideoAdListener, IBannerAdListener
{
    public static AppodealController Instance;
    public string AppKey;

    void Awake()
    {
        Instance = this; 
        DontDestroyOnLoad(transform.gameObject);

    }

	// Use this for initialization
	void Start () {
        // evHandler = new AppodealHandler();
        Appodeal.setInterstitialCallbacks(this);
        Appodeal.setNonSkippableVideoCallbacks(this);
        Appodeal.setBannerCallbacks(this);

        string appKey = AppKey; // "230d823108e7cf564c644db462a0e9ac8046e6f7cb023d11";
        Appodeal.disableLocationPermissionCheck();
        Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.NON_SKIPPABLE_VIDEO | Appodeal.BANNER_BOTTOM);

        ShowAd(Appodeal.INTERSTITIAL);
        
    }


    public void ShowAd(int _type)
    {   
        StopCoroutine("tryShowAd");
        StartCoroutine("tryShowAd", _type);
        Debug.Log("trying show ad");
    }

    IEnumerator tryShowAd(int _type)
    {        
        while (!Appodeal.isLoaded(_type))
        {           
                yield return new WaitForSeconds(0.1f);
        }       
        Appodeal.show(_type);
    }

    public void ShowBanner()
    {
        StopCoroutine("tryShowBanner");
           
        StartCoroutine("tryShowBanner");
    }

    IEnumerator tryShowBanner()
    {
        //Appodeal.hide(Appodeal.BANNER_BOTTOM);
        while (!Appodeal.isLoaded(Appodeal.BANNER_BOTTOM))
        {
            yield return new WaitForSeconds(0.1f);
        }
        Appodeal.show(Appodeal.BANNER_BOTTOM);
    }

    //callbacks
    public void onInterstitialLoaded()
    {
        Debug.Log("Interstitial loaded");
    }
    public void onInterstitialFailedToLoad()
    {
        Debug.Log("Interstitial failed");
        StopCoroutine("tryShowAd");
        LoadScript.Hide();
    }
    public void onInterstitialShown()
    {
        Debug.Log("Interstitial opened");
        //dontShow = true;
    }
    public void onInterstitialClosed()
    {
        Debug.Log("Interstitial closed");
        //dontShow = true;
        LoadScript.Hide();
    }
    public void onInterstitialClicked()
    {
        Debug.Log("Interstitial clicked");
        //dontShow = true;
        LoadScript.Hide();
    }

    //------------------
    public void onNonSkippableVideoLoaded()
    {
        Debug.Log("NonSkipableVideoLoaded");
    }
    public void onNonSkippableVideoFailedToLoad()
    {
        StopCoroutine("tryShowAd");

        Debug.Log("NonSkipableVideoFailedToLoaded");

        LoadScript.Hide();
    }
    public void onNonSkippableVideoShown()
    {
        Debug.Log("NonSkipableVideoShown");
        //dontShow = true;
    }
    public void onNonSkippableVideoFinished()
    {
        //dontShow = true;
        //LoadScript.Hide();
        GlobalData.Score = GlobalData.Score + 25;
        //SaveData.stat.SaveObj("Money");
        PlayerPrefs.SetInt("Money", GlobalData.Score);
        Debug.Log("NonSkipableVideoFinished");
    }
    public void onNonSkippableVideoClosed()
    {
        //dontShow = true;
        Debug.Log("NonSkipableVideoClosed");
        LoadScript.Hide();
    }

    //-------------------
    public void onBannerLoaded()
    {
        
    }
    public void onBannerFailedToLoad()
    {
        StopCoroutine("tryShowBanner");
       
        //LoadScript.Hide();
    }
    public void onBannerShown()
    {
        //dontShow = true;
        //LoadScript.Hide();
       
    }
    public void onBannerClicked()
    {
        //dontShow = true;
        //LoadScript.Hide();
    }

}

