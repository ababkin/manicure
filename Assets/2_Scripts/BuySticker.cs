﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuySticker : MonoBehaviour {

    [SerializeField]
    Text costText;

	// Use this for initialization
	void Start () {
	
	}


    public void SetCost(string _cost)
    {
        costText.text = _cost;
    }
	
}
