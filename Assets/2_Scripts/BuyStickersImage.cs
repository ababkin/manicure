﻿using UnityEngine;
using System.Collections;

public class BuyStickersImage : MonoBehaviour {

    ShopManager shop;
    public string NamePack;

	void Start () {
        shop = FindObjectOfType<ShopManager>();
	}

    void OnTriggerStay2D(Collider2D other) {
        if (other.tag == "trigger") {
            if (Input.touchCount>0&&Input.GetTouch(0).phase==TouchPhase.Stationary) {
                shop.BuySticker(NamePack);
            }
        }
    }
}
