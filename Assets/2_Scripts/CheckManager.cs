﻿using UnityEngine;
using System.Collections;

public class CheckManager : MonoBehaviour
{

    public int CheckScoreColor, CheckScoreTexture, CheckScoreSticker;
    public GameObject[] Stars;
    ScenarioScript scenScript;

    void Start()
    {
        scenScript = FindObjectOfType<ScenarioScript>();
        
    }

    void Update()
    {
        if (CheckScoreColor == 0 && scenScript.ModePublicBuild == 1) for (int i = 0; i < 3; i++) Stars[i].SetActive(false);
        else if (CheckScoreTexture == 0 && scenScript.ModePublicBuild == 2) for (int i = 0; i < 3; i++) Stars[i].SetActive(false);
        else if (CheckScoreSticker == 0 && scenScript.ModePublicBuild == 3) for (int i = 0; i < 3; i++) Stars[i].SetActive(false);

        switch (GlobalData.NumberFinger)
        {
            case 1:
                switch (scenScript.ModePublicBuild)
                {
                    case 1:
                        if (scenScript.ColorCheck == scenScript.Color1 || scenScript.ColorCheck == scenScript.Color2)
                        {
                            if (CheckScoreColor >= 6)
                            {
                                Stars[0].SetActive(true);
                                scenScript.CheckScoreColor[0] = 1;
                            }
                            if (CheckScoreColor >= 12)
                            {
                                Stars[1].SetActive(true);
                                scenScript.CheckScoreColor[0] = 2;
                            }
                            if (CheckScoreColor == 20)
                            {
                                Stars[2].SetActive(true);
                                scenScript.CheckScoreColor[0] = 3;
                            }
                        }
                        break;
                }
                break;
            case 2:
                switch (scenScript.ModePublicBuild)
                {
                    case 1:
                        if (scenScript.ColorCheck == scenScript.Color1 || scenScript.ColorCheck == scenScript.Color2)
                        {
                            if (CheckScoreColor >= 6)
                            {
                                Stars[0].SetActive(true);
                                scenScript.CheckScoreColor[1] = 1;
                            }
                            if (CheckScoreColor >= 12)
                            {
                                Stars[1].SetActive(true);
                                scenScript.CheckScoreColor[1] = 2;
                            }
                            if (CheckScoreColor >= 20)
                            {
                                Stars[2].SetActive(true);
                                scenScript.CheckScoreColor[1] = 3;
                            }
                        }
                        break;
                }
                break;
            case 3:
                switch (scenScript.ModePublicBuild)
                {
                    case 1:
                        if (scenScript.ColorCheck == scenScript.Color1 || scenScript.ColorCheck == scenScript.Color2)
                        {
                            if (CheckScoreColor >= 6)
                            {
                                Stars[0].SetActive(true);
                                scenScript.CheckScoreColor[2] = 1;
                            }
                            if (CheckScoreColor >= 12)
                            {
                                Stars[1].SetActive(true);
                                scenScript.CheckScoreColor[2] = 2;
                            }
                            if (CheckScoreColor >= 20)
                            {
                                Stars[2].SetActive(true);
                                scenScript.CheckScoreColor[2] = 3;
                            }
                        }
                        break;
                }
                break;
            case 4:
                switch (scenScript.ModePublicBuild)
                {
                    case 1:
                        if (scenScript.ColorCheck == scenScript.Color1 || scenScript.ColorCheck == scenScript.Color2)
                        {
                            if (CheckScoreColor >= 6)
                            {
                                Stars[0].SetActive(true);
                                scenScript.CheckScoreColor[3] = 1;
                            }
                            if (CheckScoreColor >= 12)
                            {
                                Stars[1].SetActive(true);
                                scenScript.CheckScoreColor[3] = 2;
                            }
                            if (CheckScoreColor >= 20)
                            {
                                Stars[2].SetActive(true);
                                scenScript.CheckScoreColor[3] = 3;
                            }
                        }
                        break;
                }
                break;
            case 5:
                switch (scenScript.ModePublicBuild)
                {
                    case 1:
                        if (scenScript.ColorCheck == scenScript.Color1 || scenScript.ColorCheck == scenScript.Color2)
                        {
                            if (CheckScoreColor >= 6)
                            {
                                Stars[0].SetActive(true);
                                scenScript.CheckScoreColor[4] = 1;
                            }
                            if (CheckScoreColor >= 12)
                            {
                                Stars[1].SetActive(true);
                                scenScript.CheckScoreColor[4] = 2;
                            }
                            if (CheckScoreColor >= 20)
                            {
                                Stars[2].SetActive(true);
                                scenScript.CheckScoreColor[4] = 3;
                            }
                        }
                        break;
                }
                break;
        }
    }

    public void CheckTextureAndStickers() {
        switch (GlobalData.NumberFinger) {
            case 1:
                switch (scenScript.ModePublicBuild)
                {
                    case 2:
                        if (scenScript.TextureCheck == scenScript.Texture)
                        {
                            for (int i = 0; i < 3; i++) Stars[i].SetActive(true);
                            CheckScoreTexture = 1;
                            scenScript.CheckScoreTexture[0] = 1;
                        }
                        break;
                    case 3:
                        if (scenScript.StickerCheck == scenScript.Sticker1 || scenScript.StickerCheck == scenScript.Sticker2)
                        {
                            for (int i = 0; i < 3; i++) Stars[i].SetActive(true);
                            CheckScoreSticker = 1;
                            scenScript.CheckScoreSticker[0] = 1;
                        }
                        break;
                }
                break;
            case 2:
                switch (scenScript.ModePublicBuild)
                {
                    case 2:
                        if (scenScript.TextureCheck == scenScript.Texture)
                        {
                            for (int i = 0; i < 3; i++) Stars[i].SetActive(true);
                            CheckScoreTexture = 1;
                            scenScript.CheckScoreTexture[1] = 1;
                        }
                        break;
                    case 3:
                        if (scenScript.StickerCheck == scenScript.Sticker1 || scenScript.StickerCheck == scenScript.Sticker2)
                        {
                            for (int i = 0; i < 3; i++) Stars[i].SetActive(true);
                            CheckScoreSticker = 1;
                            scenScript.CheckScoreSticker[1] = 1;
                        }
                        break;
                }
                break;
            case 3:
                switch (scenScript.ModePublicBuild)
                {
                    case 2:
                        if (scenScript.TextureCheck == scenScript.Texture)
                        {
                            for (int i = 0; i < 3; i++) Stars[i].SetActive(true);
                            CheckScoreTexture = 1;
                            scenScript.CheckScoreTexture[2] = 1;
                        }
                        break;
                    case 3:
                        if (scenScript.StickerCheck == scenScript.Sticker1 || scenScript.StickerCheck == scenScript.Sticker2)
                        {
                            for (int i = 0; i < 3; i++) Stars[i].SetActive(true);
                            CheckScoreSticker = 1;
                            scenScript.CheckScoreSticker[2] = 1;
                        }
                        break;
                }
                break;
            case 4:
                switch (scenScript.ModePublicBuild)
                {
                    case 2:
                        if (scenScript.TextureCheck == scenScript.Texture)
                        {
                            for (int i = 0; i < 3; i++) Stars[i].SetActive(true);
                            CheckScoreTexture = 1;
                            scenScript.CheckScoreTexture[3] = 1;
                        }
                        break;
                    case 3:
                        if (scenScript.StickerCheck == scenScript.Sticker1 || scenScript.StickerCheck == scenScript.Sticker2)
                        {
                            for (int i = 0; i < 3; i++) Stars[i].SetActive(true);
                            CheckScoreSticker = 1;
                            scenScript.CheckScoreSticker[3] = 1;
                        }
                        break;
                }
                break;
            case 5:
                switch (scenScript.ModePublicBuild)
                {
                    case 2:
                        if (scenScript.TextureCheck == scenScript.Texture)
                        {
                            for (int i = 0; i < 3; i++) Stars[i].SetActive(true);
                            CheckScoreTexture = 1;
                            scenScript.CheckScoreTexture[4] = 1;
                        }
                        break;
                    case 3:
                        if (scenScript.StickerCheck == scenScript.Sticker1 || scenScript.StickerCheck == scenScript.Sticker2)
                        {
                            for (int i = 0; i < 3; i++) Stars[i].SetActive(true);
                            CheckScoreSticker = 1;
                            scenScript.CheckScoreSticker[4] = 1;
                        }
                        break;
                }
                break;
        }
    }
}
