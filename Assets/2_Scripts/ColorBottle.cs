﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ColorBottle : MonoBehaviour {

    NewPaintScript paintManager;
    public static bool TargBool;
    public GameObject[] BottleColor;
    public GameObject[] TargetImage;
    private int numberTarget = 0;

    public int NumberTarget
    {
        get
        {
            return numberTarget;
        }

        set
        {
            numberTarget = value;
            updateStates();
        }
    }

    void Start () {
        paintManager = FindObjectOfType<NewPaintScript>();       
    }

    void Update() {
        updateStates();
        /*
        if (NumberTarget == 0) TargetImage[0].SetActive(true);
        else TargetImage[0].SetActive(false);
        if (NumberTarget == 1) TargetImage[1].SetActive(true);
        else TargetImage[1].SetActive(false);
        if (NumberTarget == 2) TargetImage[2].SetActive(true);
        else TargetImage[2].SetActive(false);
        if (NumberTarget == 3) TargetImage[3].SetActive(true);
        else TargetImage[3].SetActive(false);
        if (NumberTarget == 4) TargetImage[4].SetActive(true);
        else TargetImage[4].SetActive(false);
        if (NumberTarget == 5) TargetImage[5].SetActive(true);
        else TargetImage[5].SetActive(false);
        if (NumberTarget == 6) TargetImage[6].SetActive(true);
        else TargetImage[6].SetActive(false);
        if (NumberTarget == 7) TargetImage[7].SetActive(true);
        else TargetImage[7].SetActive(false);
        if (NumberTarget == 8) TargetImage[8].SetActive(true);
        else TargetImage[8].SetActive(false);
        if (NumberTarget == 9) TargetImage[9].SetActive(true);
        else TargetImage[9].SetActive(false);
        if (NumberTarget == 10) TargetImage[10].SetActive(true);
        else TargetImage[10].SetActive(false);
        if (NumberTarget == 11) TargetImage[11].SetActive(true);
        else TargetImage[11].SetActive(false);
        if (NumberTarget == 12) TargetImage[12].SetActive(true);
        else TargetImage[12].SetActive(false);
        if (NumberTarget == 13) TargetImage[13].SetActive(true);
        else TargetImage[13].SetActive(false);
        if (NumberTarget == 14) TargetImage[14].SetActive(true);
        else TargetImage[14].SetActive(false);
        if (NumberTarget == 15) TargetImage[15].SetActive(true);
        else TargetImage[15].SetActive(false);
        if (NumberTarget == 16) TargetImage[16].SetActive(true);
        else TargetImage[16].SetActive(false);
        if (NumberTarget == 17) TargetImage[17].SetActive(true);
        else TargetImage[17].SetActive(false);
        if (NumberTarget == 18) TargetImage[18].SetActive(true);
        else TargetImage[18].SetActive(false);
        if (NumberTarget == 19) TargetImage[19].SetActive(true);
        else TargetImage[19].SetActive(false);
        if (NumberTarget == 20) TargetImage[20].SetActive(true);
        else TargetImage[20].SetActive(false);
        if (NumberTarget == 21) TargetImage[21].SetActive(true);
        else TargetImage[21].SetActive(false);
        if (NumberTarget == 22) TargetImage[22].SetActive(true);
        else TargetImage[22].SetActive(false);
        if (NumberTarget == 23) TargetImage[23].SetActive(true);
        else TargetImage[23].SetActive(false);
        if (NumberTarget == 24) TargetImage[24].SetActive(true);
        else TargetImage[24].SetActive(false);
        if (NumberTarget == 25) TargetImage[25].SetActive(true);
        else TargetImage[25].SetActive(false);
        if (NumberTarget == 26) TargetImage[26].SetActive(true);
        else TargetImage[26].SetActive(false);
        if (NumberTarget == 27) TargetImage[27].SetActive(true);
        else TargetImage[27].SetActive(false);
        if (NumberTarget == 28) TargetImage[28].SetActive(true);
        else TargetImage[28].SetActive(false);
        if (NumberTarget == 29) TargetImage[29].SetActive(true);
        else TargetImage[29].SetActive(false);
        if (NumberTarget == 30) TargetImage[30].SetActive(true);
        else TargetImage[30].SetActive(false);
        if (NumberTarget == 31) TargetImage[31].SetActive(true);
        else TargetImage[31].SetActive(false);
        if (NumberTarget == 32) TargetImage[32].SetActive(true);
        else TargetImage[32].SetActive(false);
        */
    }

    void updateStates()
    {
        for (int i = 0; i < TargetImage.Length; i++)
            TargetImage[i].SetActive(false);
        TargetImage[numberTarget].SetActive(true);
    }

    public void SelectColor(int NumberColor) {
        /*
        if (SceneManager.GetActiveScene().name == "FreeMode")
            FindObjectOfType<GoogleAnal>().GooglePrefab.LogEvent("ElementID", "ColorBottle", "ColorID: " + NumberColor.ToString(), 1);*/

        if (NumberColor < 21)
        {
            paintManager.Pen.GetComponent<Image>().color = BottleColor[NumberColor].GetComponent<Image>().color;
            paintManager.TotalPen.GetComponent<Image>().color = BottleColor[NumberColor].GetComponent<Image>().color;
        } else {
            paintManager.Pen.GetComponent<Image>().color = Color.white;
            paintManager.Pen.GetComponent<Image>().sprite = BottleColor[NumberColor].transform.FindChild("sprite").GetComponent<Image>().sprite;
            paintManager.TotalPen.GetComponent<Image>().color = BottleColor[NumberColor].GetComponent<Image>().color;
            paintManager.TotalPen.GetComponent<Image>().sprite = BottleColor[NumberColor].transform.FindChild("sprite").GetComponent<Image>().sprite;
        }
        NumberTarget = NumberColor;
       if(SceneManager.GetActiveScene().name=="ScenarioMode") FindObjectOfType<ScenarioScript>().ColorCheck = NumberColor;
    }
}
