﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColorTexture : MonoBehaviour {

    public GameObject[] FingerTextureObj, SmallFingerTextureObj, ColorSprite;

    public void SetColorTexture(int NumberColor) {
        switch (GlobalData.NumberFinger) {
            case 1:
                FingerTextureObj[0].GetComponent<Image>().color = ColorSprite[NumberColor].GetComponent<Image>().color;
                SmallFingerTextureObj[0].GetComponent<Image>().color = ColorSprite[NumberColor].GetComponent<Image>().color;
                break;
            case 2:
                FingerTextureObj[1].GetComponent<Image>().color = ColorSprite[NumberColor].GetComponent<Image>().color;
                SmallFingerTextureObj[1].GetComponent<Image>().color = ColorSprite[NumberColor].GetComponent<Image>().color;
                break;
            case 3:
                FingerTextureObj[2].GetComponent<Image>().color = ColorSprite[NumberColor].GetComponent<Image>().color;
                SmallFingerTextureObj[2].GetComponent<Image>().color = ColorSprite[NumberColor].GetComponent<Image>().color;
                break;
            case 4:
                FingerTextureObj[3].GetComponent<Image>().color = ColorSprite[NumberColor].GetComponent<Image>().color;
                SmallFingerTextureObj[3].GetComponent<Image>().color = ColorSprite[NumberColor].GetComponent<Image>().color;
                break;
            case 5:
                FingerTextureObj[4].GetComponent<Image>().color = ColorSprite[NumberColor].GetComponent<Image>().color;
                SmallFingerTextureObj[4].GetComponent<Image>().color = ColorSprite[NumberColor].GetComponent<Image>().color;
                break;
        }
    }
}
