﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class DateBonus : MonoBehaviour
{

    public int FirstDay, ab, firs;
    public GameObject BonusWindow, OpenBonuses,MyADS;
    public GameObject[] BonusCases;
    public Sprite[] CaseImage;

    void Awake()
    {
        StartStep();
    }

    void Start()
    {
        if (FirstDay == DateTime.Now.Day && ab == 1)
        {
            BonusWindow.SetActive(true);
            MyADS.SetActive(true);
            BonusCases[0].GetComponent<Button>().interactable = true;
            BonusCases[1].GetComponent<Button>().interactable = false;
            BonusCases[2].GetComponent<Button>().interactable = false;
            BonusCases[3].GetComponent<Button>().interactable = false;
            BonusCases[4].GetComponent<Button>().interactable = false;
        }
        if (DateTime.Now.Day == (FirstDay + 1) && ab <= 2)
        {
            BonusWindow.SetActive(true);
            MyADS.SetActive(true);
            BonusCases[0].GetComponent<Button>().interactable = false;
            BonusCases[1].GetComponent<Button>().interactable = true;
            BonusCases[2].GetComponent<Button>().interactable = false;
            BonusCases[3].GetComponent<Button>().interactable = false;
            BonusCases[4].GetComponent<Button>().interactable = false;
            BonusCases[0].transform.FindChild("Image").GetComponent<Image>().sprite = Resources.Load("mark", typeof(Sprite)) as Sprite;
        }
        if (DateTime.Now.Day == (FirstDay + 2) && ab <= 3)
        {
            BonusWindow.SetActive(true);
            MyADS.SetActive(true);
            BonusCases[0].GetComponent<Button>().interactable = false;
            BonusCases[1].GetComponent<Button>().interactable = false;
            BonusCases[2].GetComponent<Button>().interactable = true;
            BonusCases[3].GetComponent<Button>().interactable = false;
            BonusCases[4].GetComponent<Button>().interactable = false;
            BonusCases[0].transform.FindChild("Image").GetComponent<Image>().sprite = Resources.Load("mark", typeof(Sprite)) as Sprite;
            BonusCases[1].transform.FindChild("Image").GetComponent<Image>().sprite = Resources.Load("mark", typeof(Sprite)) as Sprite;
        }
        if (DateTime.Now.Day == (FirstDay + 3) && ab <= 4)
        {
            BonusWindow.SetActive(true);
            MyADS.SetActive(true);
            BonusCases[0].GetComponent<Button>().interactable = false;
            BonusCases[1].GetComponent<Button>().interactable = false;
            BonusCases[2].GetComponent<Button>().interactable = false;
            BonusCases[3].GetComponent<Button>().interactable = true;
            BonusCases[4].GetComponent<Button>().interactable = false;
            BonusCases[0].transform.FindChild("Image").GetComponent<Image>().sprite = Resources.Load("mark", typeof(Sprite)) as Sprite;
            BonusCases[1].transform.FindChild("Image").GetComponent<Image>().sprite = Resources.Load("mark", typeof(Sprite)) as Sprite;
            BonusCases[2].transform.FindChild("Image").GetComponent<Image>().sprite = Resources.Load("mark", typeof(Sprite)) as Sprite;
        }
        if (DateTime.Now.Day == (FirstDay + 4) && ab == 5)
        {
            BonusWindow.SetActive(true);
            MyADS.SetActive(true);
            BonusCases[0].GetComponent<Button>().interactable = false;
            BonusCases[1].GetComponent<Button>().interactable = false;
            BonusCases[2].GetComponent<Button>().interactable = false;
            BonusCases[3].GetComponent<Button>().interactable = false;
            BonusCases[4].GetComponent<Button>().interactable = true;
            BonusCases[0].transform.FindChild("Image").GetComponent<Image>().sprite = Resources.Load("mark", typeof(Sprite)) as Sprite;
            BonusCases[1].transform.FindChild("Image").GetComponent<Image>().sprite = Resources.Load("mark", typeof(Sprite)) as Sprite;
            BonusCases[2].transform.FindChild("Image").GetComponent<Image>().sprite = Resources.Load("mark", typeof(Sprite)) as Sprite;
            BonusCases[3].transform.FindChild("Image").GetComponent<Image>().sprite = Resources.Load("mark", typeof(Sprite)) as Sprite;
        }

    }

    void Update()
    {
        firs = GlobalData.FirstStart;
    }

    public void OpenBonus(int Case)
    {
        switch (Case)
        {
            case 1:
                GlobalData.Score += 50;
                OpenBonuses.SetActive(true);
                OpenBonuses.transform.FindChild("Image").GetComponent<Image>().sprite = CaseImage[0];
                BonusWindow.SetActive(false);
                ab = 2;
                PlayerPrefs.SetInt("ab", ab);
                PlayerPrefs.Save();
                break;
            case 2:
                FindObjectOfType<ShopMainMenu>().OpenStickers(77);
               // OpenBonuses.SetActive(true);
                OpenBonuses.transform.FindChild("Image").GetComponent<Image>().sprite = CaseImage[1];
                BonusWindow.SetActive(false);
                ab = 3;
                PlayerPrefs.SetInt("ab", ab);
                PlayerPrefs.Save();
                break;
            case 3:
                GlobalData.Score += 75;
                OpenBonuses.SetActive(true);
                OpenBonuses.transform.FindChild("Image").GetComponent<Image>().sprite = CaseImage[2];
                BonusWindow.SetActive(false);
                ab = 4;
                PlayerPrefs.SetInt("ab", ab);
                PlayerPrefs.Save();
                break;
            case 4:
                FindObjectOfType<ShopMainMenu>().OpenStickers(78);
               // OpenBonuses.SetActive(true);
                OpenBonuses.transform.FindChild("Image").GetComponent<Image>().sprite = CaseImage[3];
                BonusWindow.SetActive(false);
                ab = 5;
                PlayerPrefs.SetInt("ab", ab);
                PlayerPrefs.Save();
                break;
            case 5:
                GlobalData.Score += 150;
                OpenBonuses.SetActive(true);
                OpenBonuses.transform.FindChild("Image").GetComponent<Image>().sprite = CaseImage[4];
                BonusWindow.SetActive(false);
                break;
        }
    }

    void StartStep()
    {
        // 
        // Debug.Log(ab);
        FirstDay = PlayerPrefs.GetInt("Day");
        if (DateTime.Now.Day > (FirstDay + 4)) GlobalData.FirstStart = 0;
        else {
            GlobalData.FirstStart = PlayerPrefs.GetInt("FirstStart");
            ab = PlayerPrefs.GetInt("ab");
        }

            if (GlobalData.FirstStart == 0)
        {
            FirstDay = DateTime.Now.Day;
            if (ab == 0) ab = 1;
            PlayerPrefs.SetInt("Day", FirstDay);
            GlobalData.FirstStart = 1;
            PlayerPrefs.SetInt("FirstStart", GlobalData.FirstStart);
            PlayerPrefs.Save();
        }
            
    }
}
