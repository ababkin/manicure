﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogScript : MonoBehaviour
{
    public Text DialogText,a;
    public GameObject DialogObject,BottonShop;
    public static int FirstBeggin;
    public Image v;

    void Start() {
        Debug.Log("First");
        FirstBeggin = PlayerPrefs.GetInt("FirstBeggin");
        Button("Next");
    }

    void Update() {
        v.enabled = true;
        if (GlobalData.ModeBuild != 3) BottonShop.SetActive(false);

        if (GlobalData.LocalizationGame == "Eng") a.text = "Perfectly! Get new coin for incredible design at the end of each level! Collect the entire collection!";
        else if (GlobalData.LocalizationGame == "Rus") a.text = "Прекрасно! Получай новые монетки за невероятный дизайн в конце каждого уровня! Собери всю коллекцию! ";
    }

    public void Button(string Name)
    {
        Debug.Log("First 001");
        if (Name == "Next")
        {
           // if (FirstBeggin == 0)
            {
                switch (GlobalData.ModeBuild)
                {
                    case 0:
                        if (!DialogObject.activeInHierarchy) DialogObject.SetActive(true);
                        if (GlobalData.LocalizationGame == "Eng") DialogText.text = "Welcome to the nail salon! Choose the nail and create your first design! ";
                        else if (GlobalData.LocalizationGame == "Rus") DialogText.text = "Добро пожаловать в наш маникюрный салон! Выбери ноготь и создай свой первый шедевр! ";
                        FindObjectOfType<GoogleAnal>().GooglePrefab.LogEvent("Training", "View", "Dialog1", 1);
                        break;
                    case 1:
                        if (!DialogObject.activeInHierarchy) DialogObject.SetActive(true);
                        if (GlobalData.LocalizationGame == "Eng") DialogText.text = "Choose a color and paint the nails!";
                        else if (GlobalData.LocalizationGame == "Rus") DialogText.text = "Для начала выбери цвет лака и раскрась ноготь! ";
                        FindObjectOfType<GoogleAnal>().GooglePrefab.LogEvent("Training", "View", "Dialog2", 1);
                        break;
                    case 2:
                        if (!DialogObject.activeInHierarchy) DialogObject.SetActive(true);
                        if (GlobalData.LocalizationGame == "Eng") DialogText.text = "Excellent! Now select the pattern and color for it! ";
                        else if (GlobalData.LocalizationGame == "Rus") DialogText.text = "Прекрасно! Теперь выбери текстуру для более лучшего результата ";
                        FindObjectOfType<GoogleAnal>().GooglePrefab.LogEvent("Training", "View", "Dialog3", 1);
                        break;
                    case 3:
                        if (!DialogObject.activeInHierarchy)
                        {
                            DialogObject.SetActive(true);
                            BottonShop.SetActive(true);
                        }
                        if (GlobalData.LocalizationGame == "Eng") DialogText.text = "Incredible !!! For end of level decorate a nail with a sticker! In the shop you will see a huge selection of fun stickers! Let's buy the first set? ";
                        else if (GlobalData.LocalizationGame == "Rus") DialogText.text = "Невероятно!!! Для завершения уровня укрась ноготь стикером! В магазине ты увидишь огромный выбор забавных наклеек! Давай купим первый набор? ";
                        FindObjectOfType<GoogleAnal>().GooglePrefab.LogEvent("Training", "View", "Dialog4", 1);
                        break;
                }
            }
        }

        if (Name == "Hand")
        {
            if (FirstBeggin == 0) {
                if (!DialogObject.activeInHierarchy) DialogObject.SetActive(true);
                if (GlobalData.LocalizationGame == "Eng") DialogText.text = "Good job! Now create a design for each nail!";
                else if (GlobalData.LocalizationGame == "Rus") DialogText.text = "Прекрасная работа! Теперь выбери следующий ноготь и продолжай в том же духе. Удачи!!! ";
                FindObjectOfType<GoogleAnal>().GooglePrefab.LogEvent("Training", "View", "Dialog5", 1);
                FirstBeggin = 1;
                PlayerPrefs.SetInt("FirstBeggin", FirstBeggin);
                PlayerPrefs.Save(); 
            }
        }
    }


}
