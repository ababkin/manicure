﻿using UnityEngine;
using System.Collections;

public class DotScript : MonoBehaviour
{
    float radius = 2.5f;
    public LayerMask WatsIsWats;
    public bool Filled, a = true;
    public CheckManager checkManager;
    ScenarioScript scenScript;


    void Start()
    {
        scenScript = FindObjectOfType<ScenarioScript>();
    }

    void FixedUpdate()
    {
        if (!Filled) Filled = Physics2D.OverlapCircle(transform.position, radius, WatsIsWats);
    }

    void Update()
    {
        switch (scenScript.ModePublicBuild)
        {
            case 1:
                if (Filled && a)
                {
                    checkManager.CheckScoreColor += 1;
                    a = false;
                }
                break;
        }
    }
    public void Af()
    {
        a = true;
    }
}
