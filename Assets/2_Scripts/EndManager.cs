﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndManager : MonoBehaviour {

    int Score;
    public GameObject[] Stars;
    public Text TitleScore;

	void Start () {
        Score = Random.Range(70, 100);
        TitleScore.text = "" + Score;
        GlobalData.Score += Score;
        if (Score <= 85) Stars[2].GetComponent<Image>().sprite = Resources.Load("Stars/StarOff",typeof(Sprite)) as Sprite;
        else if (Score > 85) Stars[2].GetComponent<Image>().sprite = Resources.Load("Stars/StarOn", typeof(Sprite)) as Sprite;
        FindObjectOfType<SaveData>().SaveObj("Money");
    }
	

	void Update () {
	
	}

    public void ButtonsUI(string ButtonName) {
        switch (ButtonName) {
            case"ShowVideo":

                break;
            case "Refresh":
                FindObjectOfType<SaveData>().SaveObj("Money");
                GlobalData.StartTap = true;
                GlobalData.FirstStart = 1;
                if(SceneManager.GetActiveScene().name=="FreeMode") SceneManager.LoadScene("FreeMode");
                else if(SceneManager.GetActiveScene().name=="ScenarioMode") SceneManager.LoadScene("ScenarioMode");
                break;
            case "GoMainMenu":
                SceneManager.LoadScene("MainMenu");
                break;
        }
    }
}
