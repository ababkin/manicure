﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Eraser : MonoBehaviour
{
    public GameObject[] Nails, TotalNails;
    public GameObject[] BrushBig, BrushLittle;
    public Image[] TextureImage,TextureTotalImage;
    public Color color;

    void Update() {
        if (GlobalData.ModeBuild == 3) gameObject.SetActive(false);
        if (SceneManager.GetActiveScene().name == "ScenarioMode") if (FindObjectOfType<ScenarioScript>().ModePublicBuild == 3) gameObject.SetActive(false);
    }

    public void ScenarioEraser() {
        if (SceneManager.GetActiveScene().name == "FreeMode") {
            switch (GlobalData.NumberFinger)
            {
                case 1:
                    if (GlobalData.ModeBuild==1)
                    {
                        Image[] abb1 = BrushBig[0].transform.GetComponentsInChildren<Image>();
                        Image[] abl1 = BrushLittle[0].transform.GetComponentsInChildren<Image>();
                        foreach (Image BrushLittleChild in abl1)
                        {
                            BrushLittleChild.enabled = false;
                        }
                        foreach (Image BrushBigChild in abb1)
                        {
                            BrushBigChild.enabled = false;
                        }
                    }
                    if (GlobalData.ModeBuild == 2)
                    {
                        Debug.Log("a");
                        Nails[0].GetComponent<Image>().sprite = null;
                        TotalNails[0].GetComponent<Image>().sprite = null;
                    }
                    break;
                case 2:
                    if (GlobalData.ModeBuild == 1)
                    {
                        Image[] abb1 = BrushBig[1].transform.GetComponentsInChildren<Image>();
                        Image[] abl1 = BrushLittle[1].transform.GetComponentsInChildren<Image>();
                        foreach (Image BrushLittleChild in abl1)
                        {
                            BrushLittleChild.enabled = false;
                        }
                        foreach (Image BrushBigChild in abb1)
                        {
                            BrushBigChild.enabled = false;
                        }
                    }
                    else if (GlobalData.ModeBuild == 2)
                    {
                        Nails[1].GetComponent<Image>().sprite = null;
                        TotalNails[1].GetComponent<Image>().sprite = null;
                    }
                    break;
                case 3:
                    if (GlobalData.ModeBuild == 1)
                    {
                        Image[] abb1 = BrushBig[2].transform.GetComponentsInChildren<Image>();
                        Image[] abl1 = BrushLittle[2].transform.GetComponentsInChildren<Image>();
                        foreach (Image BrushLittleChild in abl1)
                        {
                            BrushLittleChild.enabled = false;
                        }
                        foreach (Image BrushBigChild in abb1)
                        {
                            BrushBigChild.enabled = false;
                        }
                    }
                    else if (GlobalData.ModeBuild == 2)
                    {
                        Nails[2].GetComponent<Image>().sprite = null;
                        TotalNails[2].GetComponent<Image>().sprite = null;
                    }
                    break;
                case 4:
                    if (GlobalData.ModeBuild == 1)
                    {
                        Image[] abb1 = BrushBig[3].transform.GetComponentsInChildren<Image>();
                        Image[] abl1 = BrushLittle[3].transform.GetComponentsInChildren<Image>();
                        foreach (Image BrushLittleChild in abl1)
                        {
                            BrushLittleChild.enabled = false;
                        }
                        foreach (Image BrushBigChild in abb1)
                        {
                            BrushBigChild.enabled = false;
                        }
                    }
                    else if (GlobalData.ModeBuild == 2)
                    {
                        Nails[3].GetComponent<Image>().sprite = null;
                        TotalNails[3].GetComponent<Image>().sprite = null;
                    }
                    break;
                case 5:
                    if (GlobalData.ModeBuild == 1)
                    {
                        Image[] abb1 = BrushBig[4].transform.GetComponentsInChildren<Image>();
                        Image[] abl1 = BrushLittle[4].transform.GetComponentsInChildren<Image>();
                        foreach (Image BrushLittleChild in abl1)
                        {
                            BrushLittleChild.enabled = false;
                        }
                        foreach (Image BrushBigChild in abb1)
                        {
                            BrushBigChild.enabled = false;
                        }
                    }
                    else if (GlobalData.ModeBuild == 2)
                    {
                        Nails[4].GetComponent<Image>().sprite = null;
                        TotalNails[4].GetComponent<Image>().sprite = null;
                    }
                    break;
            }
        }
        if (SceneManager.GetActiveScene().name == "ScenarioMode") {
            switch (GlobalData.NumberFinger)
            {
                case 1:
                    if (FindObjectOfType<ScenarioScript>().ModePublicBuild == 1)
                    {
                        Image[] abb1 = BrushBig[0].transform.GetComponentsInChildren<Image>();
                        Image[] abl1 = BrushLittle[0].transform.GetComponentsInChildren<Image>();
                        foreach (Image BrushLittleChild in abl1)
                        {
                            BrushLittleChild.enabled = false;
                        }
                        foreach (Image BrushBigChild in abb1)
                        {
                            BrushBigChild.enabled = false;
                        }
                    }
                    if (FindObjectOfType<ScenarioScript>().ModePublicBuild == 2) {
                        Debug.Log("a");
                        Nails[0].GetComponent<Image>().sprite = null;
                        TotalNails[0].GetComponent<Image>().sprite = null;
                    }
                    break;
                case 2:
                    if (FindObjectOfType<ScenarioScript>().ModePublicBuild == 1)
                    {
                        Image[] abb1 = BrushBig[1].transform.GetComponentsInChildren<Image>();
                        Image[] abl1 = BrushLittle[1].transform.GetComponentsInChildren<Image>();
                        foreach (Image BrushLittleChild in abl1)
                        {
                            BrushLittleChild.enabled = false;
                        }
                        foreach (Image BrushBigChild in abb1)
                        {
                            BrushBigChild.enabled = false;
                        }
                    }
                    else if (FindObjectOfType<ScenarioScript>().ModePublicBuild == 2)
                    {
                        Nails[1].GetComponent<Image>().sprite = null;
                        TotalNails[1].GetComponent<Image>().sprite = null;
                    }
                    break;
                case 3:
                    if (FindObjectOfType<ScenarioScript>().ModePublicBuild == 1)
                    {
                        Image[] abb1 = BrushBig[2].transform.GetComponentsInChildren<Image>();
                        Image[] abl1 = BrushLittle[2].transform.GetComponentsInChildren<Image>();
                        foreach (Image BrushLittleChild in abl1)
                        {
                            BrushLittleChild.enabled = false;
                        }
                        foreach (Image BrushBigChild in abb1)
                        {
                            BrushBigChild.enabled = false;
                        }
                    }
                    else if (FindObjectOfType<ScenarioScript>().ModePublicBuild == 2)
                    {
                        Nails[2].GetComponent<Image>().sprite = null;
                        TotalNails[2].GetComponent<Image>().sprite = null;
                    }
                    break;
                case 4:
                    if (FindObjectOfType<ScenarioScript>().ModePublicBuild == 1)
                    {
                        Image[] abb1 = BrushBig[3].transform.GetComponentsInChildren<Image>();
                        Image[] abl1 = BrushLittle[3].transform.GetComponentsInChildren<Image>();
                        foreach (Image BrushLittleChild in abl1)
                        {
                            BrushLittleChild.enabled = false;
                        }
                        foreach (Image BrushBigChild in abb1)
                        {
                            BrushBigChild.enabled = false;
                        }
                    }
                    else if (FindObjectOfType<ScenarioScript>().ModePublicBuild == 2)
                    {
                        Nails[3].GetComponent<Image>().sprite = null;
                        TotalNails[3].GetComponent<Image>().sprite = null;
                    }
                    break;
                case 5:
                    if (FindObjectOfType<ScenarioScript>().ModePublicBuild == 1)
                    {
                        Image[] abb1 = BrushBig[4].transform.GetComponentsInChildren<Image>();
                        Image[] abl1 = BrushLittle[4].transform.GetComponentsInChildren<Image>();
                        foreach (Image BrushLittleChild in abl1)
                        {
                            BrushLittleChild.enabled = false;
                        }
                        foreach (Image BrushBigChild in abb1)
                        {
                            BrushBigChild.enabled = false;
                        }
                    }
                    else if (FindObjectOfType<ScenarioScript>().ModePublicBuild == 2)
                    {
                        Nails[4].GetComponent<Image>().sprite = null;
                        TotalNails[4].GetComponent<Image>().sprite = null;
                    }
                    break;
            }
        }
    }

    public void ErasserButton()
    {
        if (SceneManager.GetActiveScene().name == "ScenarioMode") return;
        switch (GlobalData.ModeBuild)
        {
            case 1:
                if (GlobalData.NumberFinger == 1)
                {
                    GameObject[] Pen = GameObject.FindGameObjectsWithTag("Pen1");
                    for (int i = 0; i < Pen.Length; i++)
                    {
                        Destroy(Pen[i].gameObject);
                    }
                }
                else if (GlobalData.NumberFinger == 2)
                {
                    GameObject[] Pen = GameObject.FindGameObjectsWithTag("Pen2");
                    for (int i = 0; i < Pen.Length; i++)
                    {
                        Destroy(Pen[i].gameObject);
                    }
                }
                else if (GlobalData.NumberFinger == 3)
                {
                    GameObject[] Pen = GameObject.FindGameObjectsWithTag("Pen3");
                    for (int i = 0; i < Pen.Length; i++)
                    {
                        Destroy(Pen[i].gameObject);
                    }
                }
                else if (GlobalData.NumberFinger == 4)
                {
                    GameObject[] Pen = GameObject.FindGameObjectsWithTag("Pen4");
                    for (int i = 0; i < Pen.Length; i++)
                    {
                        Destroy(Pen[i].gameObject);
                    }
                }
                else if (GlobalData.NumberFinger == 5)
                {
                    GameObject[] Pen = GameObject.FindGameObjectsWithTag("Pen5");
                    for (int i = 0; i < Pen.Length; i++)
                    {
                        Destroy(Pen[i].gameObject);
                    }
                }
                break;
            case 2:
                if (GlobalData.NumberFinger == 1)
                {
                    TextureImage[0].color = color;
                    TextureTotalImage[0].color = color;
                }
                else if (GlobalData.NumberFinger == 1)
                {
                    TextureImage[1].color = color;
                    TextureTotalImage[0].color = color;
                }
                else if (GlobalData.NumberFinger == 1)
                {
                    TextureImage[2].color = color;
                    TextureTotalImage[0].color = color;
                }
                else if (GlobalData.NumberFinger == 1)
                {
                    TextureImage[3].color = color;
                    TextureTotalImage[0].color = color;
                }
                else if (GlobalData.NumberFinger == 1)
                {
                    TextureImage[4].color = color;
                    TextureTotalImage[0].color = color;
                }
                break;
        }
    }
}
