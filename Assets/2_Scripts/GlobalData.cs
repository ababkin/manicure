﻿using UnityEngine;
using System.Collections;

public static class GlobalData {

    public static bool DrawMode,EraserMode;
    public static int NumberFinger;

    public static int FirstStart,DialogFirstStart;

    public static int ModeBuild;

    public static int Score;
    public static bool StartTap;
    public static bool[] BuyBool = new bool[21];

    public static string LocalizationGame;

    public static int FrenchComplite,PartyComplite,PicnicComplite,DateComplite;

    public static int NoAds = 0;
	
}
