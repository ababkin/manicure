﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GoogleAnal : MonoBehaviour
{
    public GoogleAnalyticsV3 GooglePrefab;
    public string NameScen;

    void Start()
    {
        GooglePrefab.LogScreen(NameScen);
    }

    public void GoogleLogEvent(string ButtonName)
    {
        switch (ButtonName)
        {
            case "Play":
                GooglePrefab.LogEvent("Button", "Play", "Play (Main Menu)", 1);
                break;
            case "PlayFreeMode":
                GooglePrefab.LogEvent("Button","Press (Main Menu)","PlayFreeMode",1);
                break;
            case "Shop":
                GooglePrefab.LogEvent("Button", "Shop", "Shop (Main Menu)", 1);
                break;
            case "Paint Color":
                GooglePrefab.LogEvent("Screen", "View (Free Mode)", "Screen Paint Color", 1);
                break;
            case "Paint Texture":
                GooglePrefab.LogEvent("Screen", "View (Free Mode)", "Screen Texture & Color", 1);
                break;
            case "Stickers":
                GooglePrefab.LogEvent("Screen", "View (Free Mode)", "Screen Stickers", 1);
                break;
            case "Hand Button":
                GooglePrefab.LogEvent("Button", "Press (Free Mode)", "Hand Button", 1);
                break;
            case "Eraser":
                GooglePrefab.LogEvent("Buttons", "Press (Free Mode)", "Sterka Button", 1);
                break;
            case "Finish":
                GooglePrefab.LogEvent("Buttons", "Press (Free Mode)", "Finish Button", 1);
                GooglePrefab.LogEvent("Screen", "View (Free Mode)", "Finish popap", 1);
                break;
            case "Main Menu":
                GooglePrefab.LogEvent("Button", "Press (Free Mode)", "Main Menu Button", 1);
                break;
            case "Refresh":
                GooglePrefab.LogEvent("Button", "Press (Free Mode)", "Refresh Button", 1);
                break;
            case "PlayScenarioMode":
                GooglePrefab.LogEvent("Button", "Press (Main Menu)", "Play Scenario Mode", 1);
                GooglePrefab.LogEvent("Screen", "View (Scenario Mode)", "Map", 1);
                break;
            case "French quest":
                GooglePrefab.LogEvent("Button", "Press (Scenario Mode)", "French quest", 1);
                GooglePrefab.LogEvent("Screen", "View (Scenario Mode)", "French quest", 1);
                break;
            case "Party quest":
                GooglePrefab.LogEvent("Button", "Press (Scenario Mode)", "Party quest", 1);
                GooglePrefab.LogEvent("Screen", "View (Scenario Mode)", "Party quest", 1);
                break;
            case "Picnic quest":
                GooglePrefab.LogEvent("Button", "Press (Scenario Mode)", "Picnic quest", 1);
                GooglePrefab.LogEvent("Screen", "View (Scenario Mode)", "Picnic quest", 1);
                break;
            case "Date quest":
                GooglePrefab.LogEvent("Button", "Press (Scenario Mode)", "Date quest", 1);
                GooglePrefab.LogEvent("Screen", "View (Scenario Mode)", "Date quest", 1);
                break;
            case "Random quest":
                GooglePrefab.LogEvent("Button", "Press (Scenario Mode)", "Random quest", 1);
                GooglePrefab.LogEvent("Screen", "View (Scenario Mode)", "Random quest", 1);
                break;
            case "Stage 1":
                if (SceneManager.GetActiveScene().name == "ScenarioMode") {
                    switch (FindObjectOfType<QuestManager>().LevelNumber) {
                        case 1:
                            GooglePrefab.LogEvent("Screen", "French quest", "Stage 1", 1);
                            break;
                        case 2:
                            GooglePrefab.LogEvent("Screen", "Party quest", "Stage 1", 1);
                            break;
                        case 3:
                            GooglePrefab.LogEvent("Screen", "Picnic quest", "Stage 1", 1);
                            break;
                        case 4:
                            GooglePrefab.LogEvent("Screen", "Date quest", "Stage 1", 1);
                            break;
                        case 5:
                            GooglePrefab.LogEvent("Screen", "Random quest", "Stage 1", 1);
                            break;
                    }
                }                 
                break;
            case "Stage 2":
                if (SceneManager.GetActiveScene().name == "ScenarioMode")
                {
                    switch (FindObjectOfType<QuestManager>().LevelNumber)
                    {
                        case 1:
                            GooglePrefab.LogEvent("Screen", "French quest", "Stage 2", 1);
                            break;
                        case 2:
                            GooglePrefab.LogEvent("Screen", "Party quest", "Stage 2", 1);
                            break;
                        case 3:
                            GooglePrefab.LogEvent("Screen", "Picnic quest", "Stage 2", 1);
                            break;
                        case 4:
                            GooglePrefab.LogEvent("Screen", "Date quest", "Stage 2", 1);
                            break;
                        case 5:
                            GooglePrefab.LogEvent("Screen", "Random quest", "Stage 2", 1);
                            break;
                    }
                }
                break;
            case "Stage 3":
                if (SceneManager.GetActiveScene().name == "ScenarioMode")
                {
                    switch (FindObjectOfType<QuestManager>().LevelNumber)
                    {
                        case 1:
                            GooglePrefab.LogEvent("Screen", "French quest", "Stage 3", 1);
                            break;
                        case 2:
                            GooglePrefab.LogEvent("Screen", "Party quest", "Stage 3", 1);
                            break;
                        case 3:
                            GooglePrefab.LogEvent("Screen", "Picnic quest", "Stage 3", 1);
                            break;
                        case 4:
                            GooglePrefab.LogEvent("Screen", "Date quest", "Stage 3", 1);
                            break;
                        case 5:
                            GooglePrefab.LogEvent("Screen", "Random quest", "Stage 3", 1);
                            break;
                    }
                }
                break;
            case "Help Button":
                if (SceneManager.GetActiveScene().name == "ScenarioMode")
                {
                    switch (FindObjectOfType<QuestManager>().LevelNumber)
                    {
                        case 1:
                            switch (FindObjectOfType<ScenarioScript>().ModePublicBuild) {
                                case 1:
                                    GooglePrefab.LogEvent("Button", "FrenchMode(Stage1)", "Help Button", 1);
                                    break;
                                case 2:
                                    GooglePrefab.LogEvent("Button", "FrenchMode(Stage2)", "Help Button", 1);
                                    break;
                                case 3:
                                    GooglePrefab.LogEvent("Button", "FrenchMode(Stage3)", "Help Button", 1);
                                    break;
                            }
                           
                            break;
                        case 2:
                            switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                            {
                                case 1:
                                    GooglePrefab.LogEvent("Button", "PartyMode(Stage1)", "Help Button", 1);
                                    break;
                                case 2:
                                    GooglePrefab.LogEvent("Button", "PartyMode(Stage2)", "Help Button", 1);
                                    break;
                                case 3:
                                    GooglePrefab.LogEvent("Button", "PartyMode(Stage3)", "Help Button", 1);
                                    break;
                            }
                            break;
                        case 3:
                            switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                            {
                                case 1:
                                    GooglePrefab.LogEvent("Button", "PicnicMode(Stage1)", "Help Button", 1);
                                    break;
                                case 2:
                                    GooglePrefab.LogEvent("Button", "PicnicMode(Stage2)", "Help Button", 1);
                                    break;
                                case 3:
                                    GooglePrefab.LogEvent("Button", "PicnicMode(Stage3)", "Help Button", 1);
                                    break;
                            }
                            break;
                        case 4:
                            switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                            {
                                case 1:
                                    GooglePrefab.LogEvent("Button", "DateMode(Stage1)", "Help Button", 1);
                                    break;
                                case 2:
                                    GooglePrefab.LogEvent("Button", "DateMode(Stage2)", "Help Button", 1);
                                    break;
                                case 3:
                                    GooglePrefab.LogEvent("Button", "DateMode(Stage3)", "Help Button", 1);
                                    break;
                            }
                            break;
                        case 5:
                            switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                            {
                                case 1:
                                    GooglePrefab.LogEvent("Button", "RandomMode(Stage1)", "Help Button", 1);
                                    break;
                                case 2:
                                    GooglePrefab.LogEvent("Button", "RandomMode(Stage2)", "Help Button", 1);
                                    break;
                                case 3:
                                    GooglePrefab.LogEvent("Button", "RandomMode(Stage3)", "Help Button", 1);
                                    break;
                            }
                            break;
                    }
                }
                break;
            case "Menu Button":
                if (SceneManager.GetActiveScene().name == "ScenarioMode")
                {
                    switch (FindObjectOfType<QuestManager>().LevelNumber)
                    {
                        case 1:
                            switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                            {
                                case 1:
                                    GooglePrefab.LogEvent("Button", "FrenchMode(Stage1)", "Home Button", 1);
                                    break;
                                case 2:
                                    GooglePrefab.LogEvent("Button", "FrenchMode(Stage2)", "Home Button", 1);
                                    break;
                                case 3:
                                    GooglePrefab.LogEvent("Button", "FrenchMode(Stage3)", "Home Button", 1);
                                    break;
                            }

                            break;
                        case 2:
                            switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                            {
                                case 1:
                                    GooglePrefab.LogEvent("Button", "PartyMode(Stage1)", "Home Button", 1);
                                    break;
                                case 2:
                                    GooglePrefab.LogEvent("Button", "PartyMode(Stage2)", "Home Button", 1);
                                    break;
                                case 3:
                                    GooglePrefab.LogEvent("Button", "PartyMode(Stage3)", "Home Button", 1);
                                    break;
                            }
                            break;
                        case 3:
                            switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                            {
                                case 1:
                                    GooglePrefab.LogEvent("Button", "PicnicMode(Stage1)", "Home Button", 1);
                                    break;
                                case 2:
                                    GooglePrefab.LogEvent("Button", "PicnicMode(Stage2)", "Home Button", 1);
                                    break;
                                case 3:
                                    GooglePrefab.LogEvent("Button", "PicnicMode(Stage3)", "Home Button", 1);
                                    break;
                            }
                            break;
                        case 4:
                            switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                            {
                                case 1:
                                    GooglePrefab.LogEvent("Button", "DateMode(Stage1)", "Home Button", 1);
                                    break;
                                case 2:
                                    GooglePrefab.LogEvent("Button", "DateMode(Stage2)", "Home Button", 1);
                                    break;
                                case 3:
                                    GooglePrefab.LogEvent("Button", "DateMode(Stage3)", "Home Button", 1);
                                    break;
                            }
                            break;
                        case 5:
                            switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                            {
                                case 1:
                                    GooglePrefab.LogEvent("Button", "RandomMode(Stage1)", "Home Button", 1);
                                    break;
                                case 2:
                                    GooglePrefab.LogEvent("Button", "RandomMode(Stage2)", "Home Button", 1);
                                    break;
                                case 3:
                                    GooglePrefab.LogEvent("Button", "RandomMode(Stage3)", "Home Button", 1);
                                    break;
                            }
                            break;
                    }
                }
                break;
            case "Complite quest":
                if (SceneManager.GetActiveScene().name == "ScenarioMode")
                {
                    switch (FindObjectOfType<QuestManager>().LevelNumber)
                    {
                        case 1:
                            GooglePrefab.LogEvent("Button", "Press(ScenarioMode)", "Complite French Quest", 1);
                            break;
                        case 2:
                            GooglePrefab.LogEvent("Button", "Press(ScenarioMode)", "Complite Party Quest", 1);
                            break;
                        case 3:
                            GooglePrefab.LogEvent("Button", "Press(ScenarioMode)", "Complite Picnic Quest", 1);
                            break;
                        case 4:
                            GooglePrefab.LogEvent("Button", "Press(ScenarioMode)", "Complite Date Quest", 1);
                            break;
                        case 5:
                            GooglePrefab.LogEvent("Button", "Press(ScenarioMode)", "Complite Random Quest", 1);
                            break;
                    }
                }
                break;
            case "Faild quest":
                if (SceneManager.GetActiveScene().name == "ScenarioMode")
                {
                    switch (FindObjectOfType<QuestManager>().LevelNumber)
                    {
                        case 1:
                            GooglePrefab.LogEvent("Button", "Press(ScenarioMode)", "Faild French Quest", 1);
                            break;
                        case 2:
                            GooglePrefab.LogEvent("Button", "Press(ScenarioMode)", "Faild Party Quest", 1);
                            break;
                        case 3:
                            GooglePrefab.LogEvent("Button", "Press(ScenarioMode)", "Faild Picnic Quest", 1);
                            break;
                        case 4:
                            GooglePrefab.LogEvent("Button", "Press(ScenarioMode)", "Faild Date Quest", 1);
                            break;
                        case 5:
                            GooglePrefab.LogEvent("Button", "Press(ScenarioMode)", "Faild Random Quest", 1);
                            break;
                    }
                }
                break;
            case "Share":
                GooglePrefab.LogEvent("Button", "Share (Scenario Mode)", "Share (Scenario Mode)", 1);
                break;
            case "ShareFreeMode":
                GooglePrefab.LogEvent("Button", "Share (Free Mode)", "Share (Free Mode)", 1);
                break;
            case "ShopFreeMode":
                GooglePrefab.LogEvent("Button", "Shop", "ShopFreeMode", 1);
                break;
        }
    }
}
