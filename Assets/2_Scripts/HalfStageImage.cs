﻿using UnityEngine;
using System.Collections;

public class HalfStageImage : MonoBehaviour {

    float TimerActive;
    public GameObject ViewQuest;

	void Start () {
        TimerActive = 2;
    }
	

	void Update () {
        if (TimerActive > 0) TimerActive -= Time.deltaTime;
        else if (TimerActive <= 0) {
            ViewQuest.SetActive(true);
            gameObject.SetActive(false);
            TimerActive = 2;
        }
	}
}
