﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using AppodealAds.Unity.Api;

public class LevelManager : MonoBehaviour {

    public GameObject Brush, Texture, Stickers,Hand;
    public GameObject BackButton,Eraser,NextButton,BackHand,FinishButton;
    public Text TitleScore;

    public static int NumberSession;
    public static bool StartGame;

    void Awake() {
        NumberSession = PlayerPrefs.GetInt("Session");
    }

	void Start () {
        if (!StartGame) {
            NumberSession++;
            StartGame = true;
        }
        PlayerPrefs.SetInt("Session", NumberSession);
        PlayerPrefs.Save();
	}

    void Update()
    {

        if (NumberSession == 2) {
           // FindObjectOfType<PSS>().Achievement("Comeback");
        }
        TitleScore.text = "" + GlobalData.Score;

        if(GlobalData.ModeBuild!=0) if (FinishButton.activeInHierarchy) FinishButton.SetActive(false);

        if (GlobalData.ModeBuild == 0) Hand.SetActive(true);
        else Hand.SetActive(false);
        if (GlobalData.ModeBuild == 1) Brush.SetActive(true);
        else Brush.SetActive(false);
        if (GlobalData.ModeBuild == 2) Texture.SetActive(true);
        else Texture.SetActive(false);
        if (GlobalData.ModeBuild == 3) Stickers.SetActive(true);
        else Stickers.SetActive(false);   
    }



    public void ModeDraw() {
        if (GlobalData.ModeBuild == 0)
        {
            GlobalData.ModeBuild = 1;
            NextButton.SetActive(true);
        }
        else if (GlobalData.ModeBuild == 1)
        {
            GlobalData.ModeBuild = 2;
            BackButton.SetActive(true);
            FindObjectOfType<DialogScript>().Button("Next");
            FindObjectOfType<GoogleAnal>().GoogleLogEvent("Paint Texture");
            
        }
        else if (GlobalData.ModeBuild == 2)
        {
            GlobalData.ModeBuild = 3;
            NextButton.SetActive(false);
            BackHand.SetActive(true);
            FindObjectOfType<DialogScript>().Button("Next");
            FindObjectOfType<GoogleAnal>().GoogleLogEvent("Stickers");
        }
        else if (GlobalData.ModeBuild == 3)
        {
            GlobalData.ModeBuild = 0;
            GlobalData.NumberFinger = 0;
            FinishButton.SetActive(true);
            BackHand.SetActive(false);
            BackButton.SetActive(false);
            FindObjectOfType<DialogScript>().Button("Hand");
            FindObjectOfType<GoogleAnal>().GoogleLogEvent("Hand Button"); 
            if(GlobalData.NumberFinger==3) AppodealController.Instance.ShowAd(Appodeal.INTERSTITIAL);   //FindObjectOfType<GoogleADMob>().ShowADS("FullScreen");
        }

    }
    public void Back()
    {
        if (GlobalData.ModeBuild == 3) GlobalData.ModeBuild = 2;
        else if (GlobalData.ModeBuild == 2) GlobalData.ModeBuild = 1;
    }
}
