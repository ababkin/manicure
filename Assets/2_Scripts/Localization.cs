﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Localization : MonoBehaviour {

    public Text ShareText;
    public Text a;

    void Start()
    {
        if (Application.systemLanguage == SystemLanguage.Russian) GlobalData.LocalizationGame = "Rus";
        else GlobalData.LocalizationGame = "Eng";

        if (GlobalData.LocalizationGame == "Rus")
        {
            ShareText.text = "А теперь покажи друзьям свой чудесный маникюр!";
            if (a != null) a.text = "Хочешь увидеть подсказку? Посмотри видео рекламу!";
        }
        else {
            ShareText.text = "Now show your friends your wonderful manicure!";
            if (a != null) a.text = "Want to see the hint? Watch the video advertising!";
        }
    }

}
