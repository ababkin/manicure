﻿using UnityEngine;
using System.Collections;

public class LockImage : MonoBehaviour {

    public GameObject ShopObject;
	

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "trigger")
        {
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Stationary)
            {
                ShopObject.SetActive(true);
            }
        }
    }
}
