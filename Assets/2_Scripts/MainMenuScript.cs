﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour {

    public GameObject MainMenu, Shop, SelectMode;
    public GameObject TitleScore;

	void Start () {
        GlobalData.Score = PlayerPrefs.GetInt("Money");
    }
	

	void Update () {
        TitleScore.transform.FindChild("Text").GetComponent<Text>().text = "" + GlobalData.Score;
        if (Shop.activeInHierarchy) TitleScore.SetActive(true);
        else TitleScore.SetActive(false);
	}

    public void Button(string ButtonName) {
        switch (ButtonName) {
            case "Play":

                break;
            case "FreeMode":
                StartCoroutine(Load("FreeMode"));
                break;
            case "ScenarioMode":
                StartCoroutine(Load("ScenarioMode"));
                break;
            case "Shop":

                break;
            case "Share":
                Application.OpenURL("https://play.google.com/store/apps/details?id=com.princess.nail.salon.anna.spa.polish");
                break;
        }
    }

    IEnumerator Load(string name) {
        if (name == "ScenarioMode")
        {
            Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.InversedLarge);
            Handheld.StartActivityIndicator();
            yield return new WaitForSeconds(0);
            Application.LoadLevel("ScenarioMode");
        }
        if (name == "FreeMode")
        {
            Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.InversedLarge);
            Handheld.StartActivityIndicator();
            yield return new WaitForSeconds(0);
            Application.LoadLevel("FreeMode");
        }
    }
}
