﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MapScript : MonoBehaviour {

    public GameObject[] Island;
    public GameObject[] logo;

	void Update () {
        if (GlobalData.FrenchComplite == 1)
        {
            logo[0].SetActive(true);
            Island[1].GetComponent<Image>().color = Color.white;
        }
        if (GlobalData.PartyComplite == 1)
        {
            logo[1].SetActive(true);
            Island[2].GetComponent<Image>().color = Color.white;
        }
        if (GlobalData.PicnicComplite == 1)
        {
            logo[2].SetActive(true);
            Island[3].GetComponent<Image>().color = Color.white;
        }
        if (GlobalData.DateComplite == 1)
        {
            logo[3].SetActive(true);
            Island[4].GetComponent<Image>().color = Color.white;
        }
    }
}
