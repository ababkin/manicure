﻿using UnityEngine;
using System.Collections;

public class MusicBoxScript : MonoBehaviour {

    public static bool muze;


    void Awake() {

        DontDestroyOnLoad(transform.gameObject);
    }

    void Start()
    {
        if (muze == false)
        {
            GetComponent<AudioSource>().enabled = true;
            muze = true;
        }else GetComponent<AudioSource>().enabled = false;
    }

    void Update()
    {

    }
}
