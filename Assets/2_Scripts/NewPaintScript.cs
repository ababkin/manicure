﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class NewPaintScript : MonoBehaviour {

    public GameObject[] TotalNails,Nails,Fingers;
    public GameObject TotalPen,Pen;
    public bool[] FirstBrushBool = new bool[5];
    ScenarioScript scenScript;
    

    void Start()
    {
        TotalPen = Resources.Load("Pen/PenNew") as GameObject;
        Pen = Resources.Load("Pen/PenNew") as GameObject;
    }
    public Vector2 ScaleTP = new Vector2(2.0f, 2.0f);
    public Vector2 ScaleP = new Vector2(4.0f, 4.0f);
    void Update () {

        Vector3 MousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        CamFingerManager(GlobalData.NumberFinger);

        if (Input.GetMouseButton(0)) {
            if (GlobalData.ModeBuild == 1)
            {
                GameObject TP = Instantiate(TotalPen, new Vector3(MousePos.x, MousePos.y, 0), transform.rotation) as GameObject;
                GameObject P = Instantiate(TotalPen, new Vector3(MousePos.x, MousePos.y, 0), transform.rotation) as GameObject;
                switch (GlobalData.NumberFinger) {
                    case 1:
                        TP.transform.SetParent(TotalNails[0].transform);                       
                        P.transform.SetParent(Nails[0].transform);
                        P.GetComponent<RectTransform>().localScale = ScaleP; //new Vector2(4, 4);
                        TP.GetComponent<RectTransform>().localScale = ScaleTP; // new Vector2(2, 2);
                        TP.transform.localPosition = P.transform.position*5.5f;
                        TP.tag = "Pen1";
                        P.tag = "Pen1";
                        break;
                    case 2:
                        TP.transform.SetParent(TotalNails[1].transform);
                        P.transform.SetParent(Nails[1].transform);
                        P.GetComponent<RectTransform>().localScale = ScaleP; //new Vector2(4, 4);
                        TP.GetComponent<RectTransform>().localScale = ScaleTP; // new Vector2(2, 2);
                        TP.transform.localPosition = P.transform.position * 5.5f;
                        TP.tag = "Pen2";
                        P.tag = "Pen2";
                        break;
                    case 3:
                        TP.transform.SetParent(TotalNails[2].transform);
                        P.transform.SetParent(Nails[2].transform);
                        P.GetComponent<RectTransform>().localScale = ScaleP; //new Vector2(4, 4);
                        TP.GetComponent<RectTransform>().localScale = ScaleTP; //new Vector2(2, 2);
                        TP.transform.localPosition = P.transform.position * 5.5f;
                        TP.tag = "Pen3";
                        P.tag = "Pen3";
                        break;
                    case 4:
                        TP.transform.SetParent(TotalNails[3].transform);
                        P.transform.SetParent(Nails[3].transform);
                        P.GetComponent<RectTransform>().localScale = ScaleP; //new Vector2(4, 4);
                        TP.GetComponent<RectTransform>().localScale = ScaleTP; //new Vector2(2, 2);
                        TP.transform.localPosition = P.transform.position * 5.5f;
                        TP.tag = "Pen4";
                        P.tag = "Pen4";
                        break;
                    case 5:
                        TP.transform.SetParent(TotalNails[4].transform);
                        P.transform.SetParent(Nails[4].transform);
                        P.GetComponent<RectTransform>().localScale = ScaleP; // new Vector2(4, 4);
                        TP.GetComponent<RectTransform>().localScale = ScaleTP; // new Vector2(2, 2);
                        TP.transform.localPosition = P.transform.position * 5.5f;
                        TP.tag = "Pen5";
                        P.tag = "Pen5";
                        break;
                }
            }
        }
    }
    void CamFingerManager(int NumberFinger) {
        if (NumberFinger == 0) Fingers[0].SetActive(true);
        else Fingers[0].SetActive(false);
        if (NumberFinger == 1) Fingers[1].SetActive(true);
        else Fingers[1].SetActive(false);
        if (NumberFinger == 2) Fingers[2].SetActive(true);
        else Fingers[2].SetActive(false);
        if (NumberFinger == 3) Fingers[3].SetActive(true);
        else Fingers[3].SetActive(false);
        if (NumberFinger == 4) Fingers[4].SetActive(true);
        else Fingers[4].SetActive(false);
        if (NumberFinger == 5) Fingers[5].SetActive(true);
        else Fingers[5].SetActive(false);
    }
    public void SelectFinger(int numberFinger) {
       // if (SceneManager.GetActiveScene().name != "ScenarioMode") return;
        switch (numberFinger)
        {
            case 0:
                GlobalData.NumberFinger = 0;
                break;
            case 1:
                GlobalData.NumberFinger = 1;
                if (SceneManager.GetActiveScene().name == "ScenarioMode")
                {
                    if (!FirstBrushBool[0])
                    {
                        FindObjectOfType<ScenarioScript>().Firstbrushint += 1;
                        FirstBrushBool[0] = true;
                    }
                    if (SceneManager.GetActiveScene().name == "ScenarioMode")
                    {
                        switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                        {
                            case 1:
                                GlobalData.ModeBuild = 1;
                                break;
                            case 2:
                                GlobalData.ModeBuild = 2;
                                break;
                            case 3:
                                GlobalData.ModeBuild = 3;
                                break;
                        }
                    }
                }
                break;
            case 2:
                GlobalData.NumberFinger = 2;
                if (SceneManager.GetActiveScene().name == "ScenarioMode")
                {
                    if (!FirstBrushBool[1])
                    {
                        FindObjectOfType<ScenarioScript>().Firstbrushint += 1;
                        FirstBrushBool[1] = true;
                    }
                    switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                    {
                        case 1:
                            GlobalData.ModeBuild = 1;
                            break;
                        case 2:
                            GlobalData.ModeBuild = 2;
                            break;
                        case 3:
                            GlobalData.ModeBuild = 3;
                            break;
                    }
                }
                break;
            case 3:
                GlobalData.NumberFinger = 3;
                if (SceneManager.GetActiveScene().name == "ScenarioMode")
                {
                    if (!FirstBrushBool[2])
                    {
                        FindObjectOfType<ScenarioScript>().Firstbrushint += 1;
                        FirstBrushBool[2] = true;
                    }
                    switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                    {
                        case 1:
                            GlobalData.ModeBuild = 1;
                            break;
                        case 2:
                            GlobalData.ModeBuild = 2;
                            break;
                        case 3:
                            GlobalData.ModeBuild = 3;
                            break;
                    }
                }
                break;
            case 4:
                GlobalData.NumberFinger = 4;
                if (SceneManager.GetActiveScene().name == "ScenarioMode")
                {
                    if (!FirstBrushBool[3])
                    {
                        FindObjectOfType<ScenarioScript>().Firstbrushint += 1;
                        FirstBrushBool[3] = true;
                    }
                    switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                    {
                        case 1:
                            GlobalData.ModeBuild = 1;
                            break;
                        case 2:
                            GlobalData.ModeBuild = 2;
                            break;
                        case 3:
                            GlobalData.ModeBuild = 3;
                            break;
                    }
                }
                break;
            case 5:
                GlobalData.NumberFinger = 5;
                if (SceneManager.GetActiveScene().name == "ScenarioMode")
                {
                    if (!FirstBrushBool[4])
                    {
                        FindObjectOfType<ScenarioScript>().Firstbrushint += 1;
                        FirstBrushBool[4] = true;
                    }
                    switch (FindObjectOfType<ScenarioScript>().ModePublicBuild)
                    {
                        case 1:
                            GlobalData.ModeBuild = 1;
                            break;
                        case 2:
                            GlobalData.ModeBuild = 2;
                            break;
                        case 3:
                            GlobalData.ModeBuild = 3;
                            break;
                    }
                }
                break;
        }
    }
}
