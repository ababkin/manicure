﻿using UnityEngine;
using System.Collections;

public class NoAdsBtn : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (GlobalData.NoAds == 1) gameObject.SetActive(false);
         
	}

    public void OnClick()
    {
        UnityIAPController.Instance.BuyConsumable(0);
    }
}
