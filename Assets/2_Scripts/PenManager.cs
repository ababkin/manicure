﻿using UnityEngine;
using System.Collections;

public class PenManager : MonoBehaviour {

    void Start() {
        if (GlobalData.NumberFinger == 1) transform.localScale = new Vector2(0.4f, 0.5f);
        else transform.localScale = new Vector2(0.5f, 0.5f);
    }

    public void DestrObj() {
        Destroy(gameObject);
    }
}
