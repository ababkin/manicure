// <copyright file="GPGSIds.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

///
/// This file is automatically generated DO NOT EDIT!
///
/// These are the constants defined in the Play Games Console for Game Services
/// Resources.
///


public static class PlayServiceScript
{
        public const string achievement_perfect_date = "CgkIodzDspwaEAIQCQ"; // <GPGSID>
        public const string achievement_french_style = "CgkIodzDspwaEAIQBg"; // <GPGSID>
        public const string achievement_stickers_in_your_pocket = "CgkIodzDspwaEAIQAg"; // <GPGSID>
        public const string achievement_first_manicure = "CgkIodzDspwaEAIQAQ"; // <GPGSID>
        public const string achievement_play_an_infinite_number_of_times = "CgkIodzDspwaEAIQCg"; // <GPGSID>
        public const string achievement_welcome_back = "CgkIodzDspwaEAIQBA"; // <GPGSID>
        public const string achievement_royal_picnic = "CgkIodzDspwaEAIQCA"; // <GPGSID>
        public const string achievement_the_incredible_adventure_begins = "CgkIodzDspwaEAIQCw"; // <GPGSID>
        public const string achievement_complete = "CgkIodzDspwaEAIQBQ"; // <GPGSID>
        public const string achievement_more_than_1000_coins_its_time_to_go_shopping = "CgkIodzDspwaEAIQDQ"; // <GPGSID>
        public const string achievement_begginner_shopaholic = "CgkIodzDspwaEAIQAw"; // <GPGSID>
        public const string achievement_start_the_party = "CgkIodzDspwaEAIQBw"; // <GPGSID>
        public const string achievement_more_than_500_coins_it_is_time_to_break_the_piggy_bank = "CgkIodzDspwaEAIQDA"; // <GPGSID>

}

