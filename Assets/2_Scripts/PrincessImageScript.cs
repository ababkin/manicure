﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PrincessImageScript : MonoBehaviour {


	void Update () {
        switch (GlobalData.ModeBuild) {
            case 0:
                GetComponent<Image>().sprite = Resources.Load("PrincessDialog/1", typeof(Sprite)) as Sprite;
                break;
            case 1:
                GetComponent<Image>().sprite = Resources.Load("PrincessDialog/2", typeof(Sprite)) as Sprite;
                break;
            case 2:
                GetComponent<Image>().sprite = Resources.Load("PrincessDialog/3", typeof(Sprite)) as Sprite;
                break;
            case 3:
                GetComponent<Image>().sprite = Resources.Load("PrincessDialog/4", typeof(Sprite)) as Sprite;
                break;
        }
	}
}
