﻿using UnityEngine;

public class QuestManager : MonoBehaviour {

    ScenarioScript SS;
    public GameObject View,Map;
    public int LevelNumber;

    void Start() {
        SS = FindObjectOfType<ScenarioScript>();
    }


    public void Quest(string Name) {
        switch (Name) {
            case "1":
                LevelNumber = 1;
                SS.Color1 = 6;
                SS.Color2 = 9;
                SS.Texture = 6;
                SS.Sticker1 = 18;
                SS.Sticker2 = 50;
                View.SetActive(true);
                Map.SetActive(false);
                FindObjectOfType<GoogleAnal>().GoogleLogEvent("French quest");               
                break;
            case "2":
                if (GlobalData.FrenchComplite==1)
                {
                    LevelNumber = 2;
                    SS.Color1 = 1;
                    SS.Color2 = 0;
                    SS.Texture = 10;
                    SS.Sticker1 = 112;
                    SS.Sticker2 = 113;
                    View.SetActive(true);
                    Map.SetActive(false);
                    FindObjectOfType<GoogleAnal>().GoogleLogEvent("Party quest");
                }
                break;
            case "3":
                if (GlobalData.PartyComplite == 1)
                {
                    LevelNumber = 3;
                    SS.Color1 = 2;
                    SS.Color2 = 5;
                    SS.Texture = 2;
                    SS.Sticker1 = 24;
                    SS.Sticker2 = 52;
                    View.SetActive(true);
                    Map.SetActive(false);
                    FindObjectOfType<GoogleAnal>().GoogleLogEvent("Picnic quest");
                }
                break;
            case "4":
                if (GlobalData.PicnicComplite==1)
                {
                    LevelNumber = 4;
                    SS.Color1 = 11;
                    SS.Color2 = 2;
                    SS.Texture = 9;
                    SS.Sticker1 = 44;
                    SS.Sticker2 = 57;
                    View.SetActive(true);
                    Map.SetActive(false);
                    FindObjectOfType<GoogleAnal>().GoogleLogEvent("Date quest");
                }
                break;
            case "Random":
                if (GlobalData.DateComplite == 1)
                {
                    LevelNumber = 5;
                    SS.RandomColor = Random.Range(1, 5);
                    SS.RandomTexture = Random.Range(1, 5);
                    SS.RandomSticker = Random.Range(1, 5);
                    View.SetActive(true);
                    Map.SetActive(false);
                    FindObjectOfType<GoogleAnal>().GoogleLogEvent("Random quest");
                }
                break;
            case "RandomBeggin":
                if (LevelNumber == 5)
                {
                    FindObjectOfType<ScenarioScript>().ModePublicBuild = 0;
                    SS.RandomColor = Random.Range(1, 5);
                    SS.RandomTexture = Random.Range(1, 5);
                    SS.RandomSticker = Random.Range(1, 5);
                    View.SetActive(true);
                    Map.SetActive(false);
                    FindObjectOfType<GoogleAnal>().GoogleLogEvent("Random quest");
                }
                break;
        }
    }

}
