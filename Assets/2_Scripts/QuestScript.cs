﻿using UnityEngine;
using System.Collections;

public class QuestScript : MonoBehaviour {

    bool target;
    GameObject Light;
    public GameObject ButtonPlay;

    void Start() {
        Light = transform.FindChild("Light").gameObject;
    }

    void Update()
    {

        // transform.localScale = new Vector2(1.5f, 1.5f);
        Light.SetActive(true);
        switch (gameObject.name)
        {
            case "Quest1":
                ButtonPlay.SetActive(true);
                break;
            case "Quest2":
                if (GlobalData.FrenchComplite == 1) ButtonPlay.SetActive(true);
                break;
            case "Quest3":
                if (GlobalData.PartyComplite == 1) ButtonPlay.SetActive(true);
                break;
            case "Quest4":
                if (GlobalData.PicnicComplite == 1) ButtonPlay.SetActive(true);
                break;
            case "Random":
                if (GlobalData.DateComplite == 1) ButtonPlay.SetActive(true);
                break;
        }
    }
}
