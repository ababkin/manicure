﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class QuestViewScript : MonoBehaviour {

    public GameObject Title, ColorImage, TextureImage, StickerImage, Hand, StageImage;
    public int NumberLevel, NumberStage;
    ScenarioScript SS;

    void Start() {
        SS = FindObjectOfType<ScenarioScript>();
        
    }

    void Update() {
        ViewCreator(NumberLevel, NumberStage);
        NumberLevel = FindObjectOfType<QuestManager>().LevelNumber;
        NumberStage = FindObjectOfType<ScenarioScript>().ModePublicBuild;
    }

    public void ViewCreator(int NumberLevel, int NumberStage) {
        switch (NumberLevel) {
            case 1:
                switch (NumberStage) {
                    case 1:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/French", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchColor", typeof(Sprite)) as Sprite;
                        TextureImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/frenchTexture", typeof(Sprite)) as Sprite;    
                        StickerImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchStickers", typeof(Sprite)) as Sprite;                        
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchHandStage1", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/1", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/frenchTexture", typeof(Sprite)) as Sprite;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchStickers", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.gray;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.gray;
                        ColorImage.GetComponent<Image>().color = Color.white;
                        break;
                    case 2:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/French", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchColor", typeof(Sprite)) as Sprite;
                        TextureImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/frenchTexture", typeof(Sprite)) as Sprite;
                        StickerImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchStickers", typeof(Sprite)) as Sprite;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchHandStage2", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/2", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/frenchTexture", typeof(Sprite)) as Sprite;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchStickers", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().color = Color.gray;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.gray;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.white;
                        break;
                    case 3:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/French", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchColor", typeof(Sprite)) as Sprite;
                        TextureImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/frenchTexture", typeof(Sprite)) as Sprite;
                        StickerImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchStickers", typeof(Sprite)) as Sprite;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchHandStage3", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/3", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/frenchTexture", typeof(Sprite)) as Sprite;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchStickers", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.gray;
                        ColorImage.GetComponent<Image>().color = Color.gray;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.white;
                        break;
                }
                break;
            case 2:
                switch (NumberStage)
                {
                    case 1:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/Party", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyColor", typeof(Sprite)) as Sprite;
                        TextureImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyTexture", typeof(Sprite)) as Sprite;
                        StickerImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyStickers", typeof(Sprite)) as Sprite;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/1", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/1", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyTexture", typeof(Sprite)) as Sprite;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyStickers", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.gray;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.gray;
                        ColorImage.GetComponent<Image>().color = Color.white;
                        break;
                    case 2:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/Party", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyColor", typeof(Sprite)) as Sprite;
                        TextureImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyTexture", typeof(Sprite)) as Sprite;
                        StickerImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyStickers", typeof(Sprite)) as Sprite;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/2", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/2", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyTexture", typeof(Sprite)) as Sprite;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyStickers", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().color = Color.gray;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.gray;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.white;
                        break;
                    case 3:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/Party", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyColor", typeof(Sprite)) as Sprite;
                        TextureImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyTexture", typeof(Sprite)) as Sprite;
                        StickerImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyStickers", typeof(Sprite)) as Sprite;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/3", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/3", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyTexture", typeof(Sprite)) as Sprite;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyStickers", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.gray;
                        ColorImage.GetComponent<Image>().color = Color.gray;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.white;
                        break;
                }
                break;
            case 3:
                switch (NumberStage)
                {
                    case 1:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/Picnic", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicColor", typeof(Sprite)) as Sprite;
                        TextureImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicTexture", typeof(Sprite)) as Sprite;
                        StickerImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicStickers", typeof(Sprite)) as Sprite;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/1", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/1", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicTexture", typeof(Sprite)) as Sprite;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicStickers", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.gray;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.gray;
                        ColorImage.GetComponent<Image>().color = Color.white;
                        break;
                    case 2:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/Picnic", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicColor", typeof(Sprite)) as Sprite;
                        TextureImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicTexture", typeof(Sprite)) as Sprite;
                        StickerImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicStickers", typeof(Sprite)) as Sprite;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/2", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/2", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicTexture", typeof(Sprite)) as Sprite;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicStickers", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().color = Color.gray;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.gray;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.white;
                        break;
                    case 3:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/Picnic", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicColor", typeof(Sprite)) as Sprite;
                        TextureImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicTexture", typeof(Sprite)) as Sprite;
                        StickerImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicStickers", typeof(Sprite)) as Sprite;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/3", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/3", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicTexture", typeof(Sprite)) as Sprite;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicStickers", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.gray;
                        ColorImage.GetComponent<Image>().color = Color.gray;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.white;
                        break;
                }
                break;
            case 4:
                switch (NumberStage)
                {
                    case 1:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/date", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateColor", typeof(Sprite)) as Sprite;
                        TextureImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateTexture", typeof(Sprite)) as Sprite;
                        StickerImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/DateStickers", typeof(Sprite)) as Sprite;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/1", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/1", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateTexture", typeof(Sprite)) as Sprite;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/DateStickers", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.gray;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.gray;
                        ColorImage.GetComponent<Image>().color = Color.white;
                        break;
                    case 2:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/date", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateColor", typeof(Sprite)) as Sprite;
                        TextureImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateTexture", typeof(Sprite)) as Sprite;
                        StickerImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/DateStickers", typeof(Sprite)) as Sprite;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/2", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/2", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateTexture", typeof(Sprite)) as Sprite;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/DateStickers", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().color = Color.gray;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.gray;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.white;
                        break;
                    case 3:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/date", typeof(Sprite)) as Sprite;
                        ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateColor", typeof(Sprite)) as Sprite;
                        TextureImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateTexture", typeof(Sprite)) as Sprite;
                        StickerImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/DateStickers", typeof(Sprite)) as Sprite;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/3", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/3", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateTexture", typeof(Sprite)) as Sprite;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/DateStickers", typeof(Sprite)) as Sprite;
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.gray;
                        ColorImage.GetComponent<Image>().color = Color.gray;
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.white;
                        break;
                }
                break;
            case 5:
                switch (NumberStage)
                {
                    case 1:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Free", typeof(Sprite)) as Sprite;
                        switch (SS.RandomColor)
                        {
                            case 1:
                                SS.Color1 = 6;
                                SS.Color2 = 9;
                                ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchColor", typeof(Sprite)) as Sprite;
                                break;
                            case 2:
                                SS.Color1 = 0;
                                SS.Color2 = 1;
                                ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyColor", typeof(Sprite)) as Sprite;
                                break;
                            case 3:
                                SS.Color1 = 2;
                                SS.Color2 = 5;
                                ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicColor", typeof(Sprite)) as Sprite;
                                break;
                            case 4:
                                SS.Color1 = 11;
                                SS.Color2 = 2;
                                ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateColor", typeof(Sprite)) as Sprite;
                                break;
                        }
                        ColorImage.GetComponent<Image>().color = Color.white;
                        switch (SS.RandomTexture)
                        {
                            case 1:
                                SS.Texture = 6;
                                TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/frenchTexture", typeof(Sprite)) as Sprite;
                                break;
                            case 2:
                                SS.Texture = 10;
                                TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyTexture", typeof(Sprite)) as Sprite;
                                break;
                            case 3:
                                SS.Texture = 2;
                                TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicTexture", typeof(Sprite)) as Sprite;
                                break;
                            case 4:
                                TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateTexture", typeof(Sprite)) as Sprite;
                                SS.Texture = 9;
                                break;
                        }
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.gray;
                        switch (SS.RandomSticker)
                        {
                            case 1:
                                SS.Sticker1 = 18;
                                SS.Sticker2 = 50;
                                StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchStickers", typeof(Sprite)) as Sprite;
                                break;
                            case 2:
                                SS.Sticker1 = 112;
                                SS.Sticker2 = 113;
                                StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyStickers", typeof(Sprite)) as Sprite;
                                break;
                            case 3:
                                SS.Sticker1 = 24;
                                SS.Sticker2 = 52;
                                StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicStickers", typeof(Sprite)) as Sprite;
                                break;
                            case 4:
                                SS.Sticker1 = 44;
                                SS.Sticker2 = 57;
                                StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/DateStickers", typeof(Sprite)) as Sprite;
                                break;
                        }
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.gray;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/FreeHand", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/1", typeof(Sprite)) as Sprite;
                        break;
                    case 2:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Free", typeof(Sprite)) as Sprite;
                        switch (SS.RandomColor)
                        {
                            case 1:
                                SS.Color1 = 6;
                                SS.Color2 = 9;
                                ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchColor", typeof(Sprite)) as Sprite;
                                break;
                            case 2:
                                SS.Color1 = 0;
                                SS.Color2 = 1;
                                ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyColor", typeof(Sprite)) as Sprite;
                                break;
                            case 3:
                                SS.Color1 = 2;
                                SS.Color2 = 5;
                                ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicColor", typeof(Sprite)) as Sprite;
                                break;
                            case 4:
                                SS.Color1 = 11;
                                SS.Color2 = 2;
                                ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateColor", typeof(Sprite)) as Sprite;
                                break;
                        }
                        ColorImage.GetComponent<Image>().color = Color.gray;
                        switch (SS.RandomTexture)
                        {
                            case 1:
                                SS.Texture = 6;
                                TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/frenchTexture", typeof(Sprite)) as Sprite;
                                break;
                            case 2:
                                SS.Texture = 10;
                                TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyTexture", typeof(Sprite)) as Sprite;
                                break;
                            case 3:
                                SS.Texture = 2;
                                TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicTexture", typeof(Sprite)) as Sprite;
                                break;
                            case 4:
                                TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateTexture", typeof(Sprite)) as Sprite;
                                SS.Texture = 9;
                                break;
                        }
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.white;
                        switch (SS.RandomSticker)
                        {
                            case 1:
                                SS.Sticker1 = 18;
                                SS.Sticker2 = 50;
                                StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchStickers", typeof(Sprite)) as Sprite;
                                break;
                            case 2:
                                SS.Sticker1 = 112;
                                SS.Sticker2 = 113;
                                StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyStickers", typeof(Sprite)) as Sprite;
                                break;
                            case 3:
                                SS.Sticker1 = 24;
                                SS.Sticker2 = 52;
                                StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicStickers", typeof(Sprite)) as Sprite;
                                break;
                            case 4:
                                SS.Sticker1 = 44;
                                SS.Sticker2 = 57;
                                StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/DateStickers", typeof(Sprite)) as Sprite;
                                break;
                        }
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.gray;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/FreeHand", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/2", typeof(Sprite)) as Sprite;
                        break;
                    case 3:
                        Title.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Free", typeof(Sprite)) as Sprite;
                        switch (SS.RandomColor)
                        {
                            case 1:
                                SS.Color1 = 6;
                                SS.Color2 = 9;
                                ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchColor", typeof(Sprite)) as Sprite;
                                break;
                            case 2:
                                SS.Color1 = 0;
                                SS.Color2 = 1;
                                ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyColor", typeof(Sprite)) as Sprite;
                                break;
                            case 3:
                                SS.Color1 = 2;
                                SS.Color2 = 5;
                                ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicColor", typeof(Sprite)) as Sprite;
                                break;
                            case 4:
                                SS.Color1 = 11;
                                SS.Color2 = 2;
                                ColorImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateColor", typeof(Sprite)) as Sprite;
                                break;
                        }
                        ColorImage.GetComponent<Image>().color = Color.gray;
                        switch (SS.RandomTexture)
                        {
                            case 1:
                                SS.Texture = 6;
                                TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/frenchTexture", typeof(Sprite)) as Sprite;
                                break;
                            case 2:
                                SS.Texture = 10;
                                TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyTexture", typeof(Sprite)) as Sprite;
                                break;
                            case 3:
                                SS.Texture = 2;
                                TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicTexture", typeof(Sprite)) as Sprite;
                                break;
                            case 4:
                                TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/dateTexture", typeof(Sprite)) as Sprite;
                                SS.Texture = 9;
                                break;
                        }
                        TextureImage.transform.FindChild("TextureImage").GetComponent<Image>().color = Color.gray;
                        switch (SS.RandomSticker)
                        {
                            case 1:
                                SS.Sticker1 = 18;
                                SS.Sticker2 = 50;
                                StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/French/FrenchStickers", typeof(Sprite)) as Sprite;
                                break;
                            case 2:
                                SS.Sticker1 = 112;
                                SS.Sticker2 = 113;
                                StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Party/PartyStickers", typeof(Sprite)) as Sprite;
                                break;
                            case 3:
                                SS.Sticker1 = 24;
                                SS.Sticker2 = 52;
                                StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Picnic/PicnicStickers", typeof(Sprite)) as Sprite;
                                break;
                            case 4:
                                SS.Sticker1 = 44;
                                SS.Sticker2 = 57;
                                StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/Date/DateStickers", typeof(Sprite)) as Sprite;
                                break;
                        }
                        StickerImage.transform.FindChild("StickerImage").GetComponent<Image>().color = Color.white;
                        Hand.GetComponent<Image>().sprite = Resources.Load("QuestSprite/Hand/FreeHand", typeof(Sprite)) as Sprite;
                        StageImage.GetComponent<Image>().sprite = Resources.Load("QuestSprite/3", typeof(Sprite)) as Sprite;
                        break;
                }
                break;
        }
    }
}
