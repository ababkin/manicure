﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SaveData : MonoBehaviour
{
    public GameObject s;
    public int[] SaveSticker = new int[21];
    int i;
    public static SaveData stat;
    void Awake()
    {
        Load("Money");
        Load("Stickers");
        Load("NoAds");
        
        //FindObjectOfType<ShopManager>().OpenStickersLoad();
    }

    void Start() {
		Load("Quest");
    }

    void Update()
    {
       if(SceneManager.GetActiveScene().name=="MainMenu") if (!s.activeInHierarchy) s.SetActive(true);
        if (Input.GetKeyDown(KeyCode.D)) PlayerPrefs.DeleteAll();

        if (i == 0)
        {
            Debug.Log("CreateSuper");
            if (SaveSticker[0] == 1) GlobalData.BuyBool[0] = true;
            else if (SaveSticker[0] == 0) GlobalData.BuyBool[0] = false;
            if (SaveSticker[1] == 1) GlobalData.BuyBool[1] = true;
            else if (SaveSticker[1] == 0) GlobalData.BuyBool[1] = false;
            if (SaveSticker[2] == 1) GlobalData.BuyBool[2] = true;
            else if (SaveSticker[2] == 0) GlobalData.BuyBool[2] = false;
            if (SaveSticker[3] == 1) GlobalData.BuyBool[3] = true;
            else if (SaveSticker[3] == 0) GlobalData.BuyBool[3] = false;
            if (SaveSticker[4] == 1) GlobalData.BuyBool[4] = true;
            else if (SaveSticker[4] == 0) GlobalData.BuyBool[4] = false;
            if (SaveSticker[5] == 1) GlobalData.BuyBool[5] = true;
            else if (SaveSticker[5] == 0) GlobalData.BuyBool[5] = false;
            if (SaveSticker[6] == 1) GlobalData.BuyBool[6] = true;
            else if (SaveSticker[6] == 0) GlobalData.BuyBool[6] = false;
            if (SaveSticker[7] == 1) GlobalData.BuyBool[7] = true;
            else if (SaveSticker[7] == 0) GlobalData.BuyBool[7] = false;
            if (SaveSticker[8] == 1) GlobalData.BuyBool[8] = true;
            else if (SaveSticker[8] == 0) GlobalData.BuyBool[8] = false;
            if (SaveSticker[9] == 1) GlobalData.BuyBool[9] = true;
            else if (SaveSticker[9] == 0) GlobalData.BuyBool[9] = false;
            if (SaveSticker[10] == 1) GlobalData.BuyBool[10] = true;
            else if (SaveSticker[10] == 0) GlobalData.BuyBool[10] = false;
            if (SaveSticker[11] == 1) GlobalData.BuyBool[11] = true;
            else if (SaveSticker[11] == 0) GlobalData.BuyBool[11] = false;
            if (SaveSticker[12] == 1) GlobalData.BuyBool[12] = true;
            else if (SaveSticker[12] == 0) GlobalData.BuyBool[12] = false;
            if (SaveSticker[13] == 1) GlobalData.BuyBool[13] = true;
            else if (SaveSticker[13] == 0) GlobalData.BuyBool[13] = false;
            if (SaveSticker[14] == 1) GlobalData.BuyBool[14] = true;
            else if (SaveSticker[14] == 0) GlobalData.BuyBool[14] = false;
            if (SaveSticker[15] == 1) GlobalData.BuyBool[15] = true;
            else if (SaveSticker[15] == 0) GlobalData.BuyBool[15] = false;
            if (SaveSticker[16] == 1) GlobalData.BuyBool[16] = true;
            else if (SaveSticker[16] == 0) GlobalData.BuyBool[16] = false;
            if (SaveSticker[17] == 1) GlobalData.BuyBool[17] = true;
            else if (SaveSticker[17] == 0) GlobalData.BuyBool[17] = false;
            if (SaveSticker[18] == 1) GlobalData.BuyBool[18] = true;
            else if (SaveSticker[18] == 0) GlobalData.BuyBool[18] = false;
            if (SaveSticker[19] == 1) GlobalData.BuyBool[19] = true;
            else if (SaveSticker[19] == 0) GlobalData.BuyBool[19] = false;
            if (SaveSticker[20] == 1) GlobalData.BuyBool[20] = true;
            else if (SaveSticker[20] == 0) GlobalData.BuyBool[20] = false;
            i = 1;
        }
    }


    public void SaveObj(string obj)
    {
        switch (obj)
        {
            case "NoAds":
                PlayerPrefs.SetInt("NoAds", GlobalData.NoAds);
                break;
            case "Money":
                PlayerPrefs.SetInt("Money", GlobalData.Score);
                print("Money Save: " + GlobalData.Score);
                break;
            case "Quest":
                PlayerPrefs.SetInt("FrenchQuest", GlobalData.FrenchComplite);
                PlayerPrefs.SetInt("PartyQuest", GlobalData.PartyComplite);
                PlayerPrefs.SetInt("PicnicQuest", GlobalData.PicnicComplite);
                PlayerPrefs.SetInt("DateQuest", GlobalData.DateComplite);
                PlayerPrefs.Save();
                break;
            case "Stickers":
                PlayerPrefs.SetInt("Stickers0", SaveSticker[0]);
                PlayerPrefs.SetInt("Stickers1", SaveSticker[1]);
                PlayerPrefs.SetInt("Stickers2", SaveSticker[2]);
                PlayerPrefs.SetInt("Stickers3", SaveSticker[3]);
                PlayerPrefs.SetInt("Stickers4", SaveSticker[4]);
                PlayerPrefs.SetInt("Stickers5", SaveSticker[5]);
                PlayerPrefs.SetInt("Stickers6", SaveSticker[6]);
                PlayerPrefs.SetInt("Stickers7", SaveSticker[7]);
                PlayerPrefs.SetInt("Stickers8", SaveSticker[8]);
                PlayerPrefs.SetInt("Stickers9", SaveSticker[9]);
                PlayerPrefs.SetInt("Stickers10", SaveSticker[10]);
                PlayerPrefs.SetInt("Stickers11", SaveSticker[11]);
                PlayerPrefs.SetInt("Stickers12", SaveSticker[12]);
                PlayerPrefs.SetInt("Stickers13", SaveSticker[13]);
                PlayerPrefs.SetInt("Stickers14", SaveSticker[14]);
                PlayerPrefs.SetInt("Stickers15", SaveSticker[15]);
                PlayerPrefs.SetInt("Stickers16", SaveSticker[16]);
                PlayerPrefs.SetInt("Stickers17", SaveSticker[17]);
                PlayerPrefs.SetInt("Stickers18", SaveSticker[18]);
                PlayerPrefs.SetInt("Stickers19", SaveSticker[19]);
                PlayerPrefs.SetInt("Stickers20", SaveSticker[20]);
                break;
        }
    }
    public void Load(string NameLoad)
    {
		switch (NameLoad) {
            case "NoAds":
                {
                    GlobalData.NoAds = PlayerPrefs.GetInt("NoAds");
                }
                break;
		case "Stickers":
			SaveSticker [0] = PlayerPrefs.GetInt ("Stickers0");
			SaveSticker [1] = PlayerPrefs.GetInt ("Stickers1");
			SaveSticker [2] = PlayerPrefs.GetInt ("Stickers2");
			SaveSticker [3] = PlayerPrefs.GetInt ("Stickers3");
			SaveSticker [4] = PlayerPrefs.GetInt ("Stickers4");
			SaveSticker [5] = PlayerPrefs.GetInt ("Stickers5");
			SaveSticker [6] = PlayerPrefs.GetInt ("Stickers6");
			SaveSticker [7] = PlayerPrefs.GetInt ("Stickers7");
			SaveSticker [8] = PlayerPrefs.GetInt ("Stickers8");
			SaveSticker [9] = PlayerPrefs.GetInt ("Stickers9");
			SaveSticker [10] = PlayerPrefs.GetInt ("Stickers10");
			SaveSticker [11] = PlayerPrefs.GetInt ("Stickers11");
			SaveSticker [12] = PlayerPrefs.GetInt ("Stickers12");
			SaveSticker [13] = PlayerPrefs.GetInt ("Stickers13");
			SaveSticker [14] = PlayerPrefs.GetInt ("Stickers14");
			SaveSticker [15] = PlayerPrefs.GetInt ("Stickers15");
			SaveSticker [16] = PlayerPrefs.GetInt ("Stickers16");
			SaveSticker [17] = PlayerPrefs.GetInt ("Stickers17");
			SaveSticker [18] = PlayerPrefs.GetInt ("Stickers18");
			SaveSticker [19] = PlayerPrefs.GetInt ("Stickers19");
			SaveSticker [20] = PlayerPrefs.GetInt ("Stickers20");
			break;
		case "Money":
			GlobalData.Score = PlayerPrefs.GetInt ("Money");
            print("Money Load: " + GlobalData.Score);
			break;
		case "Quest":
			GlobalData.FrenchComplite = PlayerPrefs.GetInt ("FrenchQuest");
			GlobalData.PartyComplite = PlayerPrefs.GetInt ("PartyQuest");
			GlobalData.PicnicComplite = PlayerPrefs.GetInt ("PicnicQuest");
			GlobalData.DateComplite = PlayerPrefs.GetInt ("DateQuest");
			break;
		}
    }
}


