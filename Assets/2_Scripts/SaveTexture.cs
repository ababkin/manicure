﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class SaveTexture : MonoBehaviour
{
    public Camera mainCam;
    public string SpriteSaveName;

    void Awake() {

    }

    void Start() {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S)) StartCoroutine(SaveScreenshot());

        
    }

    IEnumerator SaveScreenshot()
    {
        int sw = (int)Screen.width;
        int sh = (int)Screen.height;

        RenderTexture rt = new RenderTexture(sw, sh, 0);
        mainCam.targetTexture = rt;
        Texture2D sc = new Texture2D(sw, sh, TextureFormat.RGB24, false);
        mainCam.Render();

        yield return new WaitForSeconds(0.1f);
        Camera.main.Render();

        RenderTexture.active = rt;
        sc.ReadPixels(new Rect(0, 0, sw, sh), 0, 0);
        mainCam.targetTexture = null;
        RenderTexture.active = null;
        Destroy(rt);

        byte[] bytes = sc.EncodeToPNG();
        File.WriteAllBytes(Application.persistentDataPath/* + "/storage/cdcard0/"*/ + SpriteSaveName, bytes);
        Debug.Log("Great");
    }

    public void Save() {
        StartCoroutine(SaveScreenshot());
    }
}

