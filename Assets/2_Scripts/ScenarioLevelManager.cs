﻿using UnityEngine;
using System.Collections;

public class ScenarioLevelManager : MonoBehaviour {

    public GameObject GoToHand;
    public GameObject ColorPanel,TexturePanel,StickerPanel;
	
	void Update () {
        VisualElement();
	}

    void VisualElement() {
        switch (GlobalData.ModeBuild) {
            case 0:
                GoToHand.SetActive(false);
                ColorPanel.SetActive(false);
                TexturePanel.SetActive(false);
                StickerPanel.SetActive(false);
                break;
            case 1:
                ColorPanel.SetActive(true);
                GoToHand.SetActive(true);
                break;
            case 2:
                TexturePanel.SetActive(true);
                GoToHand.SetActive(true);
                break;
            case 3:
                StickerPanel.SetActive(true);
                GoToHand.SetActive(true);
                break;
        }
    }

    public void Button(string NameButton) {
        switch (NameButton) {
            case "GoToHand":
                FindObjectOfType<ScenarioScript>().Check();
                GlobalData.NumberFinger = 0;
                GlobalData.ModeBuild = 0;
                break;
        }
    }
}
