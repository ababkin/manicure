﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using AppodealAds.Unity.Api;
using System;

public class ScenarioScript : MonoBehaviour {
    public int Color1,Color2,Texture,Sticker1,Sticker2;                             // Переменные задания
    public int ColorCheck, TextureCheck, StickerCheck;                              // Переменные для проверки совпадения цвета, текстуры и стикеров, которые задаются во время игры игроком
    public GameObject OkeyButton,HalfStageImage,Winner,Failer;                      // Кнопки и окна режима сценария
    public GameObject[] StarFingers;                                                // Массив обектов звезд
    public int Firstbrushint;
    public int ModePublicBuild = 1;                                                 // Режим покраски ногтя
    public int RandomColor, RandomTexture, RandomSticker;                           // Переменные для случайного режима
    public int LevelScore;
    public int[] CheckScoreColor = new int[5], 
                 CheckScoreTexture = new int[5], 
                 CheckScoreSticker = new int[5];                                    // Очки за полноту закраски ногтя

    void Start () {
	
	}
	
	void Update () {
        if (Input.GetKey(KeyCode.O)) Check();
        if (Input.GetKey(KeyCode.N)) NextStage();
        if (Firstbrushint >= 5 && GlobalData.ModeBuild == 0) OkeyButton.SetActive(true);
        else OkeyButton.SetActive(false);

        if (ModePublicBuild == 2)
        {
            if (CheckScoreTexture[0] == 0) StarFingers[0].GetComponent<StarsScript>().Star = 0;
            if (CheckScoreTexture[1] == 0) StarFingers[1].GetComponent<StarsScript>().Star = 0;
            if (CheckScoreTexture[2] == 0) StarFingers[2].GetComponent<StarsScript>().Star = 0;
            if (CheckScoreTexture[3] == 0) StarFingers[3].GetComponent<StarsScript>().Star = 0;
            if (CheckScoreTexture[4] == 0) StarFingers[4].GetComponent<StarsScript>().Star = 0;
        }
        else if (ModePublicBuild == 3) {
            if (CheckScoreSticker[0] == 0) StarFingers[0].GetComponent<StarsScript>().Star = 0;
            if (CheckScoreSticker[1] == 0) StarFingers[1].GetComponent<StarsScript>().Star = 0;
            if (CheckScoreSticker[2] == 0) StarFingers[2].GetComponent<StarsScript>().Star = 0;
            if (CheckScoreSticker[3] == 0) StarFingers[3].GetComponent<StarsScript>().Star = 0;
            if (CheckScoreSticker[4] == 0) StarFingers[4].GetComponent<StarsScript>().Star = 0;
        }
    }

    public void NextStage()
    {
        for (int i = 0; i < 5; i++)
        {
            FindObjectOfType<NewPaintScript>().FirstBrushBool[i] = false;
        }
        if (ModePublicBuild == 1 && Firstbrushint >= 5)
        {
            //FindObjectOfType<GoogleADMob>().ShowADS("FullScreen");
            try
            {
#if !UNITY_EDITOR

            AppodealController.Instance.ShowAd(Appodeal.INTERSTITIAL);
            FindObjectOfType<GoogleAnal>().GoogleLogEvent("Stage 2");
#endif
            }
            catch (Exception e)
            { }
            HalfStageImage.SetActive(true);
            ModePublicBuild = 2;
            Firstbrushint = 0;
        }
        else if (ModePublicBuild == 2 && Firstbrushint >= 5)
        {
            //FindObjectOfType<GoogleADMob>().ShowADS("FullScreen");
            try
            {
#if !UNITY_EDITOR
            AppodealController.Instance.ShowAd(Appodeal.INTERSTITIAL);
            FindObjectOfType<GoogleAnal>().GoogleLogEvent("Stage 3");
#endif
            }
            catch (Exception e)
            { }
                HalfStageImage.SetActive(true);
            ModePublicBuild = 3;
            Firstbrushint = 0;            
        }



        else if (ModePublicBuild == 3 && Firstbrushint >= 5)
        {
            if (LevelScore > 5)
            {
                Winner.SetActive(true);
                FindObjectOfType<GoogleAnal>().GoogleLogEvent("Complite quest");
                /*switch (FindObjectOfType<QuestManager>().LevelNumber) {
                    case 1:
                        FindObjectOfType<PSS>().Achievement("French");
                        break;
                    case 2:
                        FindObjectOfType<PSS>().Achievement("French");
                        break;
                    case 3:
                        FindObjectOfType<PSS>().Achievement("Party");
                        break;
                    case 4:
                        FindObjectOfType<PSS>().Achievement("Date");
                        break;
                    case 5:
                        FindObjectOfType<PSS>().Achievement("Random");
                        break;
                }
                */
            }
            else if (LevelScore <= 5)
            {
                Failer.SetActive(true);
                FindObjectOfType<GoogleAnal>().GoogleLogEvent("Complite quest");
            }
        }
    }

    public void Check() {
        switch (GlobalData.NumberFinger) {
            case 1:
                switch (ModePublicBuild)
                {
                    case 1:
                        switch (CheckScoreColor[0]) {
                            case 1:
                                StarFingers[0].GetComponent<StarsScript>().Star = 1;
                                break;
                            case 2:
                                StarFingers[0].GetComponent<StarsScript>().Star = 2;
                                break;
                            case 3:
                                StarFingers[0].GetComponent<StarsScript>().Star = 3;
                                LevelScore++;
                                break;
                        }
                        break;
                    case 2:
                        if (CheckScoreTexture[0] == 1)
                        {
                            StarFingers[0].GetComponent<StarsScript>().Star = 3;
                            LevelScore++;
                        }
                        break;
                    case 3:
                        if (CheckScoreSticker[0] == 1)
                        {
                            StarFingers[0].GetComponent<StarsScript>().Star = 3;
                            LevelScore++;
                        }
                        break;
                }
                break;
            case 2:
                switch (ModePublicBuild)
                {
                    case 1:
                        switch (CheckScoreColor[1])
                        {
                            case 1:
                                StarFingers[1].GetComponent<StarsScript>().Star = 1;
                                break;
                            case 2:
                                StarFingers[1].GetComponent<StarsScript>().Star = 2;
                                break;
                            case 3:
                                StarFingers[1].GetComponent<StarsScript>().Star = 3;
                                LevelScore++;
                                break;
                        }
                        break;
                    case 2:
                        if (CheckScoreTexture[1] == 1)
                        {
                            StarFingers[1].GetComponent<StarsScript>().Star = 3;
                            LevelScore++;
                        }
                        break;
                    case 3:
                        if (CheckScoreSticker[1] == 1)
                        {
                            StarFingers[1].GetComponent<StarsScript>().Star = 3;
                            LevelScore++;
                        }
                        break;
                }
                break;
            case 3:
                switch (ModePublicBuild)
                {
                    case 1:
                        switch (CheckScoreColor[2])
                        {
                            case 1:
                                StarFingers[2].GetComponent<StarsScript>().Star = 1;
                                break;
                            case 2:
                                StarFingers[2].GetComponent<StarsScript>().Star = 2;
                                break;
                            case 3:
                                StarFingers[2].GetComponent<StarsScript>().Star = 3;
                                LevelScore++;
                                break;
                        }
                        break;
                    case 2:
                        if (CheckScoreTexture[2] == 1)
                        {
                            StarFingers[2].GetComponent<StarsScript>().Star = 3;
                            LevelScore++;
                        }
                        break;
                    case 3:
                        if (CheckScoreSticker[2] == 1)
                        {
                            StarFingers[2].GetComponent<StarsScript>().Star = 3;
                            LevelScore++;
                        }
                        break;
                }
                break;
            case 4:
                switch (ModePublicBuild)
                {
                    case 1:
                        switch (CheckScoreColor[3])
                        {
                            case 1:
                                StarFingers[3].GetComponent<StarsScript>().Star = 1;
                                break;
                            case 2:
                                StarFingers[3].GetComponent<StarsScript>().Star = 2;
                                break;
                            case 3:
                                StarFingers[3].GetComponent<StarsScript>().Star = 3;
                                LevelScore++;
                                break;
                        }
                        break;
                    case 2:
                        if (CheckScoreTexture[3] == 1)
                        {
                            StarFingers[3].GetComponent<StarsScript>().Star = 3;
                            LevelScore++;
                        }
                        break;
                    case 3:
                        if (CheckScoreSticker[3] == 1)
                        {
                            StarFingers[3].GetComponent<StarsScript>().Star = 3;
                            LevelScore++;
                        }
                        break;
                }
                break;
            case 5:
               switch (ModePublicBuild)
                {
                    case 1:
                        switch (CheckScoreColor[4])
                        {
                            case 1:
                                StarFingers[4].GetComponent<StarsScript>().Star = 1;
                                break;
                            case 2:
                                StarFingers[4].GetComponent<StarsScript>().Star = 2;
                                break;
                            case 3:
                                StarFingers[4].GetComponent<StarsScript>().Star = 3;
                                LevelScore++;
                                break;
                        }
                        break;
                    case 2:
                        if (TextureCheck==Texture)
                        {
                            StarFingers[4].GetComponent<StarsScript>().Star = 3;
                            LevelScore++;
                        }
                        break;
                    case 3:
                        if (CheckScoreSticker[4] == 1)
                        {
                            StarFingers[4].GetComponent<StarsScript>().Star = 3;
                            LevelScore++;
                        }
                        break;
                }
                break;

        }
        
    }

    public void Button(string NameButton)
    {
        switch (NameButton)
        {
            case "Refresh":
                SceneManager.LoadScene("ScenarioMode");
                break;
            case "MainMenu":
                SceneManager.LoadScene("MainMenu");
                break;
            case "TryAgain":
                SceneManager.LoadScene("ScenarioMode");
                break;
            case "NextLevel":
                switch (FindObjectOfType<QuestManager>().LevelNumber)
                {
                    case 1:
                        GlobalData.FrenchComplite = 1;
                        FindObjectOfType<SaveData>().SaveObj("Quest");
                        PlayerPrefs.SetInt("FrenchComplite", GlobalData.FrenchComplite);
                        SceneManager.LoadScene("ScenarioMode");
                        break;
                    case 2:
                        GlobalData.PartyComplite = 1;
                        FindObjectOfType<SaveData>().SaveObj("Quest");
                        PlayerPrefs.SetInt("PartyComplite", GlobalData.PartyComplite);
                        FindObjectOfType<Share>().ButtonShare();
                        SceneManager.LoadScene("ScenarioMode");
                        break;
                    case 3:
                        GlobalData.PicnicComplite = 1;
                        FindObjectOfType<SaveData>().SaveObj("Quest");
                        PlayerPrefs.SetInt("PicnicComplite", GlobalData.PicnicComplite);
                        SceneManager.LoadScene("ScenarioMode");
                        break;
                    case 4:
                        GlobalData.DateComplite = 1;
                        FindObjectOfType<SaveData>().SaveObj("Quest");
                        PlayerPrefs.SetInt("DateComplite", GlobalData.DateComplite);
                        SceneManager.LoadScene("ScenarioMode");
                        break;
                    case 5:
                        SceneManager.LoadScene("ScenarioMode");
                        break;
                }
                break;
        }
    }
}
