﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScriptText : MonoBehaviour {

    public GameObject[] TexturePlane;
    public bool Target;

	void Start () {
	
	}
	

	void Update () {
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "trigger") Target = true;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "trigger" && Target) {
            switch (GlobalData.NumberFinger)
            {
                case 1:
                    TexturePlane[0].GetComponent<SpriteRenderer>().color = gameObject.GetComponent<Image>().color;
                    break;
                case 2:
                    TexturePlane[1].GetComponent<SpriteRenderer>().color = gameObject.GetComponent<Image>().color;
                    break;
                case 3:
                    TexturePlane[2].GetComponent<SpriteRenderer>().color = gameObject.GetComponent<Image>().color;
                    break;
                case 4:
                    TexturePlane[3].GetComponent<SpriteRenderer>().color = gameObject.GetComponent<Image>().color;
                    break;
                case 5:
                    TexturePlane[4].GetComponent<SpriteRenderer>().color = gameObject.GetComponent<Image>().color;
                    break;
            }

        }
        //if (other.tag == "trigger" && Target) paintManager.Pen.GetComponent<SpriteRenderer>().color = gameObject.GetComponent<Image>().color;
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "trigger") Target = false;
    }
}
