﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour {


    public GameObject BuyStickerImage,NoMoneyImage,Shop;

    GameObject[] Stickers;
    public GameObject[] Windows,Sea, Butterfly, Sweet,
        Dino, Plume, Flowers, 
        Bows, Fruits, Love, 
        StarrySky, Gems, Crown,
        Star, Roses, Monsters,
        Animals, MarineLife, Owl,
        Princess, Cats, Emoji,ImageSticker;

    void Start()
    {
        #region FindGameObjects
        /* Sea = GameObject.FindGameObjectsWithTag("Sea");
         Butterfly = GameObject.FindGameObjectsWithTag("ButterFly");
         Sweet = GameObject.FindGameObjectsWithTag("Sweet");
         Dino = GameObject.FindGameObjectsWithTag("Dino");
         Plume = GameObject.FindGameObjectsWithTag("Plume");
         Flowers = GameObject.FindGameObjectsWithTag("Flowers");
         Bows = GameObject.FindGameObjectsWithTag("Bows");
         Fruits = GameObject.FindGameObjectsWithTag("Fruits");
         Love = GameObject.FindGameObjectsWithTag("Love");
         StarrySky = GameObject.FindGameObjectsWithTag("StarrySky");
         Gems = GameObject.FindGameObjectsWithTag("Gems");
         Crown = GameObject.FindGameObjectsWithTag("Crown");
         Star = GameObject.FindGameObjectsWithTag("Star");
         Roses = GameObject.FindGameObjectsWithTag("Roses");
         Monsters = GameObject.FindGameObjectsWithTag("Monsters");
         Animals = GameObject.FindGameObjectsWithTag("Animals");
         MarineLife = GameObject.FindGameObjectsWithTag("MarineLife");
         Owl = GameObject.FindGameObjectsWithTag("Owl");
         Princess = GameObject.FindGameObjectsWithTag("Princess");
         Cats = GameObject.FindGameObjectsWithTag("Cats");
         Emoji = GameObject.FindGameObjectsWithTag("Emoji");*/
        #endregion
        OpenStickersLoad();

    }

    void Update () {
	
	}

    public void OpenStickersLoad() {
        if (GlobalData.BuyBool[0]) {
            for (int i = 0; i < Sea.Length; i++)
            {
                GlobalData.BuyBool[0] = true;
                ImageSticker[0].GetComponent<Image>().color = Color.gray;
                ImageSticker[0].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Sea", typeof(Sprite)) as Sprite;
                Sea[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[1])
        {
            for (int i = 0; i < Butterfly.Length; i++)
            {
                GlobalData.BuyBool[1] = true;
                ImageSticker[1].GetComponent<Image>().color = Color.gray;
                ImageSticker[1].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/ButterFly", typeof(Sprite)) as Sprite;
                Butterfly[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[2])
        {
            for (int i = 0; i < Sweet.Length; i++)
            {
                GlobalData.BuyBool[2] = true;
                ImageSticker[2].GetComponent<Image>().color = Color.gray;
                ImageSticker[2].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Sweet", typeof(Sprite)) as Sprite;
                Sweet[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[3])
        {
            for (int i = 0; i < Dino.Length; i++)
            {
                GlobalData.BuyBool[3] = true;
                ImageSticker[3].GetComponent<Image>().color = Color.gray;
                ImageSticker[3].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Dino", typeof(Sprite)) as Sprite;
                Dino[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[4])
        {
            for (int i = 0; i < Plume.Length; i++)
            {
                GlobalData.BuyBool[4] = true;
                ImageSticker[4].GetComponent<Image>().color = Color.gray;
                ImageSticker[4].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Plume", typeof(Sprite)) as Sprite;
                Plume[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[5])
        {
            for (int i = 0; i < Flowers.Length; i++)
            {
                GlobalData.BuyBool[5] = true;
                ImageSticker[5].GetComponent<Image>().color = Color.gray;
                ImageSticker[5].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Flowers", typeof(Sprite)) as Sprite;
                Flowers[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[6])
        {
            for (int i = 0; i < Bows.Length; i++)
            {
                GlobalData.BuyBool[6] = true;
                ImageSticker[6].GetComponent<Image>().color = Color.gray;
                ImageSticker[6].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Bows", typeof(Sprite)) as Sprite;
                Bows[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[7])
        {
            for (int i = 0; i < Fruits.Length; i++)
            {
                GlobalData.BuyBool[7] = true;
                ImageSticker[7].GetComponent<Image>().color = Color.gray;
                ImageSticker[7].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Fruits", typeof(Sprite)) as Sprite;
                Fruits[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[8])
        {
            for (int i = 0; i < Love.Length; i++)
            {
                GlobalData.BuyBool[8] = true;
                ImageSticker[8].GetComponent<Image>().color = Color.gray;
                ImageSticker[8].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Love", typeof(Sprite)) as Sprite;
                Love[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[9])
        {
            for (int i = 0; i < StarrySky.Length; i++)
            {
                GlobalData.BuyBool[9] = true;
                ImageSticker[9].GetComponent<Image>().color = Color.gray;
                ImageSticker[9].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/StarrySky", typeof(Sprite)) as Sprite;
                StarrySky[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[10])
        {
            for (int i = 0; i < Gems.Length; i++)
            {
                GlobalData.BuyBool[10] = true;
                ImageSticker[10].GetComponent<Image>().color = Color.gray;
                ImageSticker[10].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Gems", typeof(Sprite)) as Sprite;
                Gems[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[11])
        {
            for (int i = 0; i < Crown.Length; i++)
            {
                GlobalData.BuyBool[11] = true;
                ImageSticker[11].GetComponent<Image>().color = Color.gray;
                ImageSticker[11].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Crown", typeof(Sprite)) as Sprite;
                Crown[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[12])
        {
            for (int i = 0; i < Star.Length; i++)
            {
                GlobalData.BuyBool[12] = true;
                ImageSticker[12].GetComponent<Image>().color = Color.gray;
                ImageSticker[12].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Star", typeof(Sprite)) as Sprite;
                Star[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[13])
        {
            for (int i = 0; i < Roses.Length; i++)
            {
                GlobalData.BuyBool[13] = true;
                ImageSticker[13].GetComponent<Image>().color = Color.gray;
                ImageSticker[13].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Roses", typeof(Sprite)) as Sprite;
                Roses[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[14])
        {
            for (int i = 0; i < Monsters.Length; i++)
            {
                GlobalData.BuyBool[14] = true;
                ImageSticker[14].GetComponent<Image>().color = Color.gray;
                ImageSticker[14].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Monsters", typeof(Sprite)) as Sprite;
                Monsters[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[15])
        {
            for (int i = 0; i < Animals.Length; i++)
            {
                GlobalData.BuyBool[15] = true;
                ImageSticker[15].GetComponent<Image>().color = Color.gray;
                ImageSticker[15].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Animals", typeof(Sprite)) as Sprite;
                Animals[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[16])
        {
            for (int i = 0; i < MarineLife.Length; i++)
            {
                GlobalData.BuyBool[16] = true;
                ImageSticker[16].GetComponent<Image>().color = Color.gray;
                ImageSticker[16].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/MarineLife", typeof(Sprite)) as Sprite;
                MarineLife[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[17])
        {
            for (int i = 0; i < Owl.Length; i++)
            {
                GlobalData.BuyBool[17] = true;
                ImageSticker[17].GetComponent<Image>().color = Color.gray;
                ImageSticker[17].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Owl", typeof(Sprite)) as Sprite;
                Owl[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[18])
        {
            for (int i = 0; i < Princess.Length; i++)
            {
                GlobalData.BuyBool[18] = true;
                ImageSticker[18].GetComponent<Image>().color = Color.grey;
                ImageSticker[18].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Princess", typeof(Sprite)) as Sprite;
                Princess[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[19])
        {
            for (int i = 0; i < Cats.Length; i++)
            {
                GlobalData.BuyBool[19] = true;
                ImageSticker[19].GetComponent<Image>().color = Color.gray;
                ImageSticker[19].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Cats", typeof(Sprite)) as Sprite;
                Cats[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
        if (GlobalData.BuyBool[20])
        {
            for (int i = 0; i < Emoji.Length; i++)
            {
                GlobalData.BuyBool[20] = true;
                ImageSticker[20].GetComponent<Image>().color = Color.gray;
                ImageSticker[20].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Emoji", typeof(Sprite)) as Sprite;
                Emoji[i].transform.FindChild("Image").gameObject.SetActive(false);
            }
        }
    }

    public void OnBuySticker(int _id)
    {
        switch (_id)
        {
            case 2: //sweet
                {
                    FindObjectOfType<SaveData>().SaveSticker[2] = 1;
                    GlobalData.BuyBool[2] = true;
                    ImageSticker[2].GetComponent<Image>().color = Color.gray;
                    ImageSticker[2].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                    BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Sweet", typeof(Sprite)) as Sprite;
                    for (int i = 0; i < 6; i++)
                        Sweet[i].transform.Find("Image").gameObject.SetActive(false);
                    Windows[0].SetActive(false);
                    Windows[1].SetActive(true);
                    FindObjectOfType<SaveData>().SaveObj("Stickers");
                    FindObjectOfType<SaveData>().SaveObj("Money");
                }
                break;
            case 5: //flower
                {
                    FindObjectOfType<SaveData>().SaveSticker[5] = 1;
                    GlobalData.BuyBool[5] = true;
                    ImageSticker[5].GetComponent<Image>().color = Color.gray;
                    ImageSticker[5].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                    BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Flowers", typeof(Sprite)) as Sprite;
                    for (int i = 0; i < 5; i++)
                        Flowers[i].transform.Find("Image").gameObject.SetActive(false);
                    Windows[0].SetActive(false);
                    Windows[1].SetActive(true);
                    FindObjectOfType<SaveData>().SaveObj("Stickers");
                    FindObjectOfType<SaveData>().SaveObj("Money");
                }
                break;
            case 8: //love
                {
                    FindObjectOfType<SaveData>().SaveSticker[8] = 1;
                    GlobalData.BuyBool[8] = true;
                    ImageSticker[8].GetComponent<Image>().color = Color.gray;
                    ImageSticker[8].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                    BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Love", typeof(Sprite)) as Sprite;
                    for (int i = 0; i < 5; i++)
                        Love[i].transform.Find("Image").gameObject.SetActive(false);
                    Windows[0].SetActive(false);
                    Windows[1].SetActive(true);
                    FindObjectOfType<SaveData>().SaveObj("Stickers");
                    FindObjectOfType<SaveData>().SaveObj("Money");
                }
                break;
        }
    }

    public void BuySticker(string ButtonName) {
        switch (ButtonName) {
            case "Tutorial":
                if (GlobalData.Score >= 50)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        FindObjectOfType<SaveData>().SaveSticker[0] = 1;
                        GlobalData.BuyBool[0] = true;
                        ImageSticker[0].GetComponent<Image>().color = Color.gray;
                        ImageSticker[0].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                        BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Sea", typeof(Sprite)) as Sprite;
                        Sea[i].transform.Find("Image").gameObject.SetActive(false);
                        Windows[0].SetActive(false);
                        Windows[1].SetActive(true);
                    }
                    GlobalData.Score -= 50;
                    FindObjectOfType<SaveData>().SaveObj("Money");
                    FindObjectOfType<SaveData>().SaveObj("Stickers");
                }
                break;
            case "Bonus2":
                for (int i = 0; i < 4; i++)
                {
                    FindObjectOfType<SaveData>().SaveSticker[19] = 1;
                    GlobalData.BuyBool[19] = true;
                    ImageSticker[19].GetComponent<Image>().color = Color.gray;
                    ImageSticker[19].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                    Cats[i].transform.Find("Image").gameObject.SetActive(false);
                    //Windows[1].SetActive(true);
                }
                break;
            case "Bonus4":
                for (int i = 0; i < 4; i++) {
                    FindObjectOfType<SaveData>().SaveSticker[18] = 1;
                    GlobalData.BuyBool[18] = true;
                    ImageSticker[18].GetComponent<Image>().color = Color.grey;
                    ImageSticker[18].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                    Princess[i].transform.Find("Image").gameObject.SetActive(false);
                    //Windows[1].SetActive(true);
                }

                for (int i = 0; i < 5; i++) {
                    FindObjectOfType<SaveData>().SaveSticker[17] = 1;
                    GlobalData.BuyBool[17] = true;
                    ImageSticker[17].GetComponent<Image>().color = Color.gray;
                    ImageSticker[17].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                    Owl[i].transform.Find("Image").gameObject.SetActive(false);
                }
                break;
            case "Sea":
                if (GlobalData.Score >= 50)
                {
                    if (!GlobalData.BuyBool[0]) {
                        for (int i = 0; i < 5; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[0] = 1;
                            GlobalData.BuyBool[0] = true;
                            ImageSticker[0].GetComponent<Image>().color = Color.gray;
                            ImageSticker[0].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Sea", typeof(Sprite)) as Sprite;
                            Sea[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);
                        }
                    }
                    GlobalData.Score -= 50;
                    FindObjectOfType<SaveData>().SaveObj("Money");
                    FindObjectOfType<SaveData>().SaveObj("Stickers");
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "ButterFly":
                if (GlobalData.Score >= 250)
                {
                    if (!GlobalData.BuyBool[1])
                    {
                        for (int i = 0; i < 7; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[1] = 1;
                            GlobalData.BuyBool[1] = true;
                            ImageSticker[1].GetComponent<Image>().color = Color.gray;
                            ImageSticker[1].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/ButterFly", typeof(Sprite)) as Sprite;
                            Butterfly[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }

                        GlobalData.Score -= 250;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Sweet":
                /*if (GlobalData.Score >= 250)
                {
                    if (!GlobalData.BuyBool[2])
                    {
                        for (int i = 0; i < 6; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[2] = 1;
                            GlobalData.BuyBool[2] = true;
                            ImageSticker[2].GetComponent<Image>().color = Color.gray;
                            ImageSticker[2].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Sweet", typeof(Sprite)) as Sprite;
                            Sweet[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);
                            
                        }
                        GlobalData.Score -= 250;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                */
                UnityIAPController.Instance.BuyConsumable(1);
                break;
            case "Dino":
                if (GlobalData.Score >= 400)
                {
                    if (!GlobalData.BuyBool[3]) { 
                        for (int i = 0; i < 6; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[3] = 1;
                            GlobalData.BuyBool[3] = true;
                            ImageSticker[3].GetComponent<Image>().color = Color.gray;
                            ImageSticker[3].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Dino", typeof(Sprite)) as Sprite;
                            Dino[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);
                        }
                    GlobalData.Score -= 400;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Plume":
                if (GlobalData.Score >= 400)
                {
                    if (!GlobalData.BuyBool[4])
                    {
                        for (int i = 0; i < 9; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[4] = 1;
                            GlobalData.BuyBool[4] = true;
                            ImageSticker[4].GetComponent<Image>().color = Color.gray;
                            ImageSticker[4].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Plume", typeof(Sprite)) as Sprite;
                            Plume[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 400;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Flowers":
                /*if (GlobalData.Score >= 400)
                {
                    if (!GlobalData.BuyBool[5])
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[5] = 1;
                            GlobalData.BuyBool[5] = true;
                            ImageSticker[5].GetComponent<Image>().color = Color.gray;
                            ImageSticker[5].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Flowers", typeof(Sprite)) as Sprite;
                            Flowers[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 400;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                */
                UnityIAPController.Instance.BuyConsumable(2);
                break;
            case "Bows":
                if (GlobalData.Score >= 600)
                {
                    if (!GlobalData.BuyBool[6])
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[6] = 1;
                            GlobalData.BuyBool[6] = true;
                            ImageSticker[6].GetComponent<Image>().color = Color.gray;
                            ImageSticker[6].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Bows", typeof(Sprite)) as Sprite;
                            Bows[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 600;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Fruits":
                if (GlobalData.Score >= 600)
                {
                    if (!GlobalData.BuyBool[7])
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[7] = 1;
                            GlobalData.BuyBool[7] = true;
                            ImageSticker[7].GetComponent<Image>().color = Color.gray;
                            ImageSticker[7].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Fruits", typeof(Sprite)) as Sprite;
                            Fruits[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 600;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Love":
                /*if (GlobalData.Score >= 600)
                {
                    if (!GlobalData.BuyBool[8])
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[8] = 1;
                            GlobalData.BuyBool[8] = true;
                            ImageSticker[8].GetComponent<Image>().color = Color.gray;
                            ImageSticker[8].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Love", typeof(Sprite)) as Sprite;
                            Love[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 600;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                */
                UnityIAPController.Instance.BuyConsumable(3);
                break;
            case "StarrySky":
                if (GlobalData.Score >= 800)
                {
                    if (!GlobalData.BuyBool[9])
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[9] = 1;
                            GlobalData.BuyBool[9] = true;
                            ImageSticker[9].GetComponent<Image>().color = Color.gray;
                            ImageSticker[9].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/StarrySky", typeof(Sprite)) as Sprite;
                            StarrySky[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 800;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Gems":
                if (GlobalData.Score >= 800)
                {
                    if (!GlobalData.BuyBool[10])
                    {
                        for (int i = 0; i < 9; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[10] = 1;
                            GlobalData.BuyBool[10] = true;
                            ImageSticker[10].GetComponent<Image>().color = Color.gray;
                            ImageSticker[10].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Gems", typeof(Sprite)) as Sprite;
                            Gems[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 800;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Crown":
                if (GlobalData.Score >= 800)
                {
                    if (!GlobalData.BuyBool[11])
                    {
                        for (int i = 0; i < 6; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[11] = 1;
                            GlobalData.BuyBool[11] = true;
                            ImageSticker[11].GetComponent<Image>().color = Color.gray;
                            ImageSticker[11].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Crown", typeof(Sprite)) as Sprite;
                            Crown[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 800;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Star":
                if (GlobalData.Score >= 850)
                {
                    if (!GlobalData.BuyBool[12])
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[12] = 1;
                            GlobalData.BuyBool[12] = true;
                            ImageSticker[12].GetComponent<Image>().color = Color.gray;
                            ImageSticker[12].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Star", typeof(Sprite)) as Sprite;
                            Star[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 850;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Roses":
                if (GlobalData.Score >= 850)
                {
                    if (!GlobalData.BuyBool[13])
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[13] = 1;
                            GlobalData.BuyBool[13] = true;
                            ImageSticker[13].GetComponent<Image>().color = Color.gray;
                            ImageSticker[13].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Roses", typeof(Sprite)) as Sprite;
                            Roses[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 850;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Monsters":
                if (GlobalData.Score >= 850)
                {
                    if (!GlobalData.BuyBool[14])
                    {
                        for (int i = 0; i < 6; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[14] = 1;
                            GlobalData.BuyBool[14] = true;
                            ImageSticker[14].GetComponent<Image>().color = Color.gray;
                            ImageSticker[14].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Monsters", typeof(Sprite)) as Sprite;
                            Monsters[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);
                        }
                        GlobalData.Score -= 850;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Animals":
                if (GlobalData.Score >= 1020)
                {
                    if (!GlobalData.BuyBool[15])
                    {
                        for (int i = 0; i < 6; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[15] = 1;
                            GlobalData.BuyBool[15] = true;
                            ImageSticker[15].GetComponent<Image>().color = Color.gray;
                            ImageSticker[15].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Animals", typeof(Sprite)) as Sprite;
                            Animals[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);
                        }
                        GlobalData.Score -= 1020;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "MarineLife":
                if (GlobalData.Score >= 1020)
                {
                    if (!GlobalData.BuyBool[16])
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[16] = 1;
                            GlobalData.BuyBool[16] = true;
                            ImageSticker[16].GetComponent<Image>().color = Color.gray;
                            ImageSticker[16].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/MarineLife", typeof(Sprite)) as Sprite;
                            MarineLife[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 1020;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
                
            case "Owl":
                if (GlobalData.Score >= 1020)
                {
                    if (!GlobalData.BuyBool[17])
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[17] = 1;
                            GlobalData.BuyBool[17] = true;
                            ImageSticker[17].GetComponent<Image>().color = Color.gray;
                            ImageSticker[17].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Owl", typeof(Sprite)) as Sprite;
                            Owl[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 1020;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Princess":
                if (GlobalData.Score >= 1500)
                {
                    if (!GlobalData.BuyBool[18])
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[18] = 1;
                            GlobalData.BuyBool[18] = true;
                            ImageSticker[18].GetComponent<Image>().color = Color.grey;
                            ImageSticker[18].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Princess", typeof(Sprite)) as Sprite;
                            Princess[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);
                        }
                        GlobalData.Score -= 1500;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Cats":
                if (GlobalData.Score >= 1500)
                {
                    if (!GlobalData.BuyBool[19])
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[19] = 1;
                            GlobalData.BuyBool[19] = true;
                            ImageSticker[19].GetComponent<Image>().color = Color.gray;
                            ImageSticker[19].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Cats", typeof(Sprite)) as Sprite;
                            Cats[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 1500;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
            case "Emoji":
                if (GlobalData.Score >= 1500)
                {
                    if (!GlobalData.BuyBool[20])
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            FindObjectOfType<SaveData>().SaveSticker[20] = 1;
                            GlobalData.BuyBool[20] = true;
                            ImageSticker[20].GetComponent<Image>().color = Color.gray;
                            ImageSticker[20].transform.FindChild("Image").GetComponent<Image>().enabled = false;
                            BuyStickerImage.GetComponent<Image>().sprite = Resources.Load("StickersNormal/Emoji", typeof(Sprite)) as Sprite;
                            Emoji[i].transform.Find("Image").gameObject.SetActive(false);
                            Windows[0].SetActive(false);
                            Windows[1].SetActive(true);

                        }
                        GlobalData.Score -= 1500;
                        FindObjectOfType<SaveData>().SaveObj("Stickers");
                        FindObjectOfType<SaveData>().SaveObj("Money");
                    }
                }
                else NoMoneyImage.SetActive(true);
                break;
        }
    }
}
