﻿using UnityEngine;
using System.Collections;

public class ShopMenu : MonoBehaviour {

    [SerializeField]
    private BuySticker sweetSticker;

    [SerializeField]
    private BuySticker flowerSticker;

    [SerializeField]
    private BuySticker loveSticker;


    // Use this for initialization
    void Start () {
	
	}


    public void Show()
    {
        Debug.Log("ShowShopMenu");
        gameObject.SetActive(true);
        sweetSticker.SetCost(UnityIAPController.Instance.GetItemPrice/* Price*/(1));
        flowerSticker.SetCost(UnityIAPController.Instance.GetItemPrice(2));
        loveSticker.SetCost(UnityIAPController.Instance.GetItemPrice(3));
    }

}
