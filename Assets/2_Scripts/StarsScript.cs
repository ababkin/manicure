﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StarsScript : MonoBehaviour {

    public int Star,NumberFinger;
    public GameObject[] StarsImage;
    
	
	void Start () {
	
	}
	
	
	void Update () {
        switch (Star) {
            case 0:
                StarsImage[0].GetComponent<Image>().sprite = Resources.Load("NoStar", typeof(Sprite)) as Sprite;
                StarsImage[1].GetComponent<Image>().sprite = Resources.Load("NoStar", typeof(Sprite)) as Sprite;
                StarsImage[2].GetComponent<Image>().sprite = Resources.Load("NoStar", typeof(Sprite)) as Sprite;
                break;
            case 1:
                StarsImage[0].GetComponent<Image>().sprite = Resources.Load("Star", typeof(Sprite)) as Sprite;
                break;
            case 2:
                StarsImage[0].GetComponent<Image>().sprite = Resources.Load("Star", typeof(Sprite)) as Sprite;
                StarsImage[1].GetComponent<Image>().sprite = Resources.Load("Star", typeof(Sprite)) as Sprite;
                break;
            case 3:
                StarsImage[0].GetComponent<Image>().sprite = Resources.Load("Star", typeof(Sprite)) as Sprite;
                StarsImage[1].GetComponent<Image>().sprite = Resources.Load("Star", typeof(Sprite)) as Sprite;
                StarsImage[2].GetComponent<Image>().sprite = Resources.Load("Star", typeof(Sprite)) as Sprite;
                break;
        }
	}
}
