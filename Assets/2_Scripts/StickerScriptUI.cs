﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class StickerScriptUI : MonoBehaviour {

    GameObject SpriteRes;
    public GameObject[] TotalNails, Nails;

    GameObject sticker, smallSticker;

    void Start() {
        SpriteRes = Resources.Load("Sticker/Sticker") as GameObject;
    }

    void Update() {
        if (smallSticker != null&&GlobalData.NumberFinger!=0)
        {
            smallSticker.transform.localPosition = sticker.transform.localPosition/5;
            smallSticker.transform.localScale = sticker.transform.localScale/4;
            smallSticker.transform.localRotation = sticker.transform.localRotation;
        }
        
    }

    public void GiveSticker(int NumberSticker) {
        try
        {
            if (SceneManager.GetActiveScene().name == "FreeMode") FindObjectOfType<GoogleAnal>().GooglePrefab.LogEvent("ElementID", "Stickers", "StickerID: " + NumberSticker.ToString(), 1);
            if (SceneManager.GetActiveScene().name == "ScenarioMode") FindObjectOfType<ScenarioScript>().StickerCheck = NumberSticker;
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
        sticker = Instantiate(SpriteRes, Vector3.zero, transform.rotation) as GameObject;
        smallSticker = Instantiate(SpriteRes, Vector3.zero, transform.rotation) as GameObject;   
        sticker.GetComponent<Image>().sprite = gameObject.GetComponent<Image>().sprite;
        smallSticker.GetComponent<Image>().sprite = gameObject.GetComponent<Image>().sprite;
        if (SceneManager.GetActiveScene().name == "ScenarioMode") FindObjectOfType<CheckManager>().CheckTextureAndStickers();
        switch (GlobalData.NumberFinger)
        {
            case 1:
                smallSticker.transform.SetParent(TotalNails[0].transform);
                sticker.transform.parent = Nails[0].transform;
                sticker.GetComponent<RectTransform>().localScale = new Vector2(2f, 2f);    
                break;
            case 2:
                smallSticker.transform.SetParent(TotalNails[1].transform);
                sticker.transform.parent = Nails[1].transform;
                sticker.GetComponent<RectTransform>().localScale = new Vector2(2f, 2f);
                break;
            case 3:
                smallSticker.transform.SetParent(TotalNails[2].transform);
                sticker.transform.parent = Nails[2].transform;
                sticker.GetComponent<RectTransform>().localScale = new Vector2(2f, 2f);
                break;
            case 4:
                smallSticker.transform.SetParent(TotalNails[3].transform);
                sticker.transform.parent = Nails[3].transform;
                sticker.GetComponent<RectTransform>().localScale = new Vector2(2f, 2f);
                break;
            case 5:
                smallSticker.transform.SetParent(TotalNails[4].transform);
                sticker.transform.parent = Nails[4].transform;
                sticker.GetComponent<RectTransform>().localScale = new Vector2(2f, 2f);
                break;
        }
    }
}
