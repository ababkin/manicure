﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StickerSprite : MonoBehaviour {

    public bool edit;
    float scale;
    Vector2 pos1, pos2;
    Vector3 mousePos,touchPos;

    void Start() {
    }

    void Update()
    {
        if (GlobalData.ModeBuild == 3)
        {
            if (edit)
            {
                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                ScaleSticker();
                if (Input.touchCount > 1) transform.localScale = new Vector2(scale / 10, scale / 10);
            }
        }
    }

    void ScaleSticker()
    {
        if (Input.touchCount == 1) transform.position = new Vector3(mousePos.x, mousePos.y, 0);
        else if (Input.touchCount > 1)
        {
            pos1 = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            pos2 = Camera.main.ScreenToWorldPoint(Input.GetTouch(1).position);
            scale = Vector2.Distance(pos1, pos2);
        }
    }

    public void MoveSticker(bool EditMode) {
        if (EditMode) edit = true;
        else if (!EditMode) edit = false;
    }
}
