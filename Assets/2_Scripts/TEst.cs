﻿using UnityEngine;
using System.Collections;

public class TEst : MonoBehaviour {

    public GameObject decalPrefab;
    public bool OnHit;

    void Start()
    {
    }

    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            if (hit.collider.tag == "Finish") OnHit = true;

            if (hit.collider.tag == "Finish" && Input.GetMouseButtonDown(0))
                DecalGen(hit.point, hit.normal, hit.collider);
        }
        else OnHit = false;
    }

    GameObject DecalGen(Vector3 p, Vector3 n, Collider c)
    {
        GameObject decalInst;

        decalInst = (GameObject)Instantiate(decalPrefab, p, Quaternion.FromToRotation(Vector3.up, n));

        MeshFilter mf = decalInst.GetComponent(typeof(MeshFilter)) as MeshFilter;
        Mesh m = mf.mesh;

        Vector3[] verts = m.vertices;

        for (int i = 0; i < verts.Length; i++)
        {
            verts[i] = decalInst.transform.TransformPoint(verts[i]);

            if (verts[i].x > c.bounds.max.x)
            {
                verts[i].x = c.bounds.max.x;
            }

            if (verts[i].x < c.bounds.min.x)
            {
                verts[i].x = c.bounds.min.x;
            }

            if (verts[i].y > c.bounds.max.y)
            {
                verts[i].y = c.bounds.max.y;
            }

            if (verts[i].y < c.bounds.min.y)
            {
                verts[i].y = c.bounds.min.y;
            }

            if (verts[i].z > c.bounds.max.z)
            {
                verts[i].z = c.bounds.max.z;
            }

            if (verts[i].z < c.bounds.min.z)
            {
                verts[i].z = c.bounds.min.z;
            }
            verts[i] = decalInst.transform.InverseTransformPoint(verts[i]);
            m.vertices = verts;
        }
        return decalInst;
    }
}
