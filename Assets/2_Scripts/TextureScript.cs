﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class TextureScript : MonoBehaviour
{
    public GameObject[] FingerTextureObj, SmallFingerTextureObj;
    //public Slider TextureScaler;

    void Update()
    {

        for (int i = 0; i < 5; i++)
        {
            SmallFingerTextureObj[i].transform.localScale = FingerTextureObj[i].transform.localScale;
            SmallFingerTextureObj[i].transform.localPosition = FingerTextureObj[i].transform.localPosition/4;
        }

       /* switch (GlobalData.NumberFinger)
        {
            case 1:
                FingerTextureObj[0].transform.localScale = new Vector2(TextureScaler.value, TextureScaler.value);
                break;
            case 2:
                FingerTextureObj[1].transform.localScale = new Vector2(TextureScaler.value, TextureScaler.value);
                break;
            case 3:
                FingerTextureObj[2].transform.localScale = new Vector2(TextureScaler.value, TextureScaler.value);
                break;
            case 4:
                FingerTextureObj[3].transform.localScale = new Vector2(TextureScaler.value, TextureScaler.value);
                break;
            case 5:
                FingerTextureObj[4].transform.localScale = new Vector2(TextureScaler.value, TextureScaler.value);
                break;
        }*/
    }

    public void CreateTexture(int NumberTexture)
    {
#if !UNITY_EDITOR
        try
        {
        if (SceneManager.GetActiveScene().name == "FreeMode") FindObjectOfType<GoogleAnal>().GooglePrefab.LogEvent("ElementID", "Texture", "TextureID: " + NumberTexture.ToString(), 1);
        if (SceneManager.GetActiveScene().name == "ScenarioMode") FindObjectOfType<ScenarioScript>().TextureCheck = NumberTexture;
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
#endif
        switch (GlobalData.NumberFinger)
        {
            case 1:
                FingerTextureObj[0].GetComponent<Image>().sprite = Resources.Load("NewTexture/" + NumberTexture, typeof(Sprite)) as Sprite;
                SmallFingerTextureObj[0].GetComponent<Image>().sprite = Resources.Load("NewTexture/" + NumberTexture, typeof(Sprite)) as Sprite;
                FingerTextureObj[0].GetComponent<Image>().color = Color.white;
                SmallFingerTextureObj[0].GetComponent<Image>().color = Color.white;
                if (SceneManager.GetActiveScene().name == "ScenarioMode") FindObjectOfType<CheckManager>().CheckTextureAndStickers();
                break;
            case 2:
                FingerTextureObj[1].GetComponent<Image>().sprite = Resources.Load("NewTexture/" + NumberTexture, typeof(Sprite)) as Sprite;
                SmallFingerTextureObj[1].GetComponent<Image>().sprite = Resources.Load("NewTexture/" + NumberTexture, typeof(Sprite)) as Sprite;
                FingerTextureObj[1].GetComponent<Image>().color = Color.white;
                SmallFingerTextureObj[1].GetComponent<Image>().color = Color.white;
                if (SceneManager.GetActiveScene().name == "ScenarioMode") FindObjectOfType<CheckManager>().CheckTextureAndStickers();
                break;
            case 3:
                FingerTextureObj[2].GetComponent<Image>().sprite = Resources.Load("NewTexture/" + NumberTexture, typeof(Sprite)) as Sprite;
                SmallFingerTextureObj[2].GetComponent<Image>().sprite = Resources.Load("NewTexture/" + NumberTexture, typeof(Sprite)) as Sprite;
                FingerTextureObj[2].GetComponent<Image>().color = Color.white;
                SmallFingerTextureObj[2].GetComponent<Image>().color = Color.white;
                if (SceneManager.GetActiveScene().name == "ScenarioMode") FindObjectOfType<CheckManager>().CheckTextureAndStickers();
                break;
            case 4:
                FingerTextureObj[3].GetComponent<Image>().sprite = Resources.Load("NewTexture/" + NumberTexture, typeof(Sprite)) as Sprite;
                SmallFingerTextureObj[3].GetComponent<Image>().sprite = Resources.Load("NewTexture/" + NumberTexture, typeof(Sprite)) as Sprite;
                FingerTextureObj[3].GetComponent<Image>().color = Color.white;
                SmallFingerTextureObj[3].GetComponent<Image>().color = Color.white;
                if (SceneManager.GetActiveScene().name == "ScenarioMode") FindObjectOfType<CheckManager>().CheckTextureAndStickers();
                break;
            case 5:
                FingerTextureObj[4].GetComponent<Image>().sprite = Resources.Load("NewTexture/" + NumberTexture, typeof(Sprite)) as Sprite;
                SmallFingerTextureObj[4].GetComponent<Image>().sprite = Resources.Load("NewTexture/" + NumberTexture, typeof(Sprite)) as Sprite;
                FingerTextureObj[4].GetComponent<Image>().color = Color.white;
                SmallFingerTextureObj[4].GetComponent<Image>().color = Color.white;
                if (SceneManager.GetActiveScene().name == "ScenarioMode") FindObjectOfType<CheckManager>().CheckTextureAndStickers();
                break;
        }
    }
    
}
