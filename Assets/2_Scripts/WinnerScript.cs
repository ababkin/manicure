﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WinnerScript : MonoBehaviour {

    public GameObject[] Stars;
    public GameObject Button;

    void Start()
    {
        if (FindObjectOfType<ScenarioScript>().LevelScore <= 5)
        {
            Stars[0].GetComponent<Image>().sprite = Resources.Load("Star", typeof(Sprite)) as Sprite;
            Stars[1].GetComponent<Image>().sprite = Resources.Load("NoStar", typeof(Sprite)) as Sprite;
            Stars[2].GetComponent<Image>().sprite = Resources.Load("NoStar", typeof(Sprite)) as Sprite;
        }
        else if (FindObjectOfType<ScenarioScript>().LevelScore > 5&& FindObjectOfType<ScenarioScript>().LevelScore < 10)
        {
            Stars[0].GetComponent<Image>().sprite = Resources.Load("Star", typeof(Sprite)) as Sprite;
            Stars[1].GetComponent<Image>().sprite = Resources.Load("Star", typeof(Sprite)) as Sprite;
            Stars[2].GetComponent<Image>().sprite = Resources.Load("NoStar", typeof(Sprite)) as Sprite;
        }

        else if (FindObjectOfType<ScenarioScript>().LevelScore >= 10)
        {
            Stars[0].GetComponent<Image>().sprite = Resources.Load("Star", typeof(Sprite)) as Sprite;
            Stars[1].GetComponent<Image>().sprite = Resources.Load("Star", typeof(Sprite)) as Sprite;
            Stars[2].GetComponent<Image>().sprite = Resources.Load("Star", typeof(Sprite)) as Sprite;
        }
    }
}
