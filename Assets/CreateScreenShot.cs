﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class CreateScreenShot : MonoBehaviour {

    public Camera PhotoCamera;
    public string path,mainPath;
    public string cName, DirectoryName = "Screen";
    public bool showText;
    int i = 0;

    void Start() {
        path = Application.persistentDataPath + "/";
        mainPath = path + DirectoryName + "/";

        if (!Directory.Exists(mainPath)) Directory.CreateDirectory(mainPath);
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.U)) StartCoroutine(SaveScreen(cName));
    }

    IEnumerator SaveScreen(string Name) {

        int sw = (int)Screen.width;
        int sh = (int)Screen.height;

        RenderTexture rt = new RenderTexture(sw, sh, 0);
        PhotoCamera.targetTexture = rt;
        Texture2D sc = new Texture2D(sw, sh, TextureFormat.RGB24, false);
        PhotoCamera.Render();

        RenderTexture.active = rt;
        sc.ReadPixels(new Rect(0, 0, sw, sh), 0, 0);
        PhotoCamera.targetTexture = null;
        RenderTexture.active = null;
        Destroy(rt);

        byte[] bytes = sc.EncodeToPNG();
        string FinalPath = mainPath + name + ".png";
        File.WriteAllBytes(FinalPath, bytes);

        yield return new WaitForSeconds(0.1f);
        gameObject.SetActive(false);
    }




}
