﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using AppodealAds.Unity.Api;

public class LoadScript : MonoBehaviour {

    public static LoadScript Instance;

	void Start(){
        Instance = this;

#if UNITY_EDITOR
        print("Load script active editor!");
        gameObject.SetActive(false);
#endif

        if (Application.internetReachability.ToString () != "NotReachable")
        {
            Debug.Log("ShowFull");
			StartCoroutine ("ShowFull",7f);
		}
        else
        {
            Debug.Log("NotReachable");
			gameObject.SetActive (false);
		}

	}

	void Update(){
		//if (FindObjectOfType<GoogleADMob> ().IsClosed) {
		//	gameObject.SetActive (false);
		//}
	}

    public static void Hide()
    {
        if (Instance != null)
        Instance.gameObject.SetActive(false);
    }

	IEnumerator ShowFull (float time) {
		yield return new WaitForSeconds (time);

        Debug.Log("LetShowInterstitialAd");
        if (GlobalData.NoAds == 1)
        {
            gameObject.SetActive(false);
        }
        else
        {
            //FindObjectOfType<GoogleADMob> ().ShowADS ("FullScreen");
            AppodealController.Instance.ShowAd(Appodeal.INTERSTITIAL);
        }

    }

    void OnDisable()
    {
        //покажем банер
        if (GlobalData.NoAds == 0)
        AppodealController.Instance.ShowBanner();
        Debug.Log("LoadWindowDisabled");
    }

    void OnDestroy()
    {
        Debug.Log("destroy LoadScript");
        Instance = null;
    }
}
