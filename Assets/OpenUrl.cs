﻿using UnityEngine;
using System.Collections;

public class OpenUrl : MonoBehaviour {

    public string URL;

    public void OpenWeb() {
        Application.OpenURL(URL);
    }
}
