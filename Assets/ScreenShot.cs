﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class ScreenShot : MonoBehaviour {

    public bool controlable = false;
    public float moveSpeed = 1;
    public int resolution = 3; // 1= default, 2= 2x default, etc.
    public string imageName = "Screenshot_",Message;
    string customPath; // leave blank for project file location
    public bool resetIndex = false;

    public Text text;

    public GameObject[] HideObject;
    public GameObject Logo;

    private int index = 0;

    void Awake()
    {
#if UNITY_EDITOR
        customPath = "C:/Users/Данил/Desktop/UnityScreeShot/";
#elif UNITY_ANDROID
        customPath = Application.persistentDataPath + "/ScreenShots/";
#endif

        if (resetIndex) PlayerPrefs.SetInt("ScreenshotIndex", 0);
        if (customPath != "")
        {
            if (!System.IO.Directory.Exists(customPath))
            {
                System.IO.Directory.CreateDirectory(customPath);
            }
        }
        index = PlayerPrefs.GetInt("ScreenshotIndex") != 0 ? PlayerPrefs.GetInt("ScreenshotIndex") : 1;
    }

    void Start() {
       if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "test") text.text = "" + customPath;
    }

    void LateUpdate() {
        if (Input.GetKeyDown(KeyCode.L)) StartCoroutine(Shots());
    }

    IEnumerator Shots()
    {

        for (int i = 0; i < 2; i++) HideObject[i].SetActive(false);
        Logo.SetActive(true);
        customPath = Path.Combine(Application.persistentDataPath, System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".png");
        Application.CaptureScreenshot(customPath, resolution);
        index++;
        Debug.Log("Screenshot saved: " + customPath + " --- " + imageName + index);

        if (!Application.isEditor)
        {
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
            AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + customPath);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);

            intentObject.Call<AndroidJavaObject>("setType", "text/plain");
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "" + Message);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), "SUBJECT");

            intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

            currentActivity.Call("startActivity", intentObject);
        }
        yield return new WaitForSeconds(0.1f);
        Logo.SetActive(false);
        for (int i = 0; i < HideObject.Length; i++) HideObject[i].SetActive(true);
    }

    void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("ScreenshotIndex", (index));
    }

    public void PhotoShot() {
            StartCoroutine(Shots());
    }
}
