﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class Share : MonoBehaviour {

    public string Url;
    private bool isProcessing = false;
    public Image[] ImageButton;
    public GameObject[] HideObj;
    string Message;

    void Start() {
        if (SceneManager.GetActiveScene().name == "ScenarioMode")
        {
            if (Application.systemLanguage == SystemLanguage.Russian) Message = "Я придумала новый дизайн маникюра! Тебе нравится? ♥ Играй вместе со мной бесплатно на Google play!♥♥♥" + Url.ToString();
			else Message = "I came up with a new design manicure! Do you like? ♥ Play with me for free on Google play!♥♥♥" + Url.ToString(); ;
        }
        else if (SceneManager.GetActiveScene().name == "FreeMode") {
			if (Application.systemLanguage == SystemLanguage.Russian) Message = "Я придумала новый дизайн маникюра! Тебе нравится? ♥ Играй вместе со мной бесплатно на Google play! ♥" + Url.ToString();
			else Message = "I came up with a new design! Do you like it? Play with me in the game! ♥" + Url.ToString(); ;
        }
    }

    public void ButtonShare()
    {
        for (int i = 0; i < 2; i++) HideObj[i].SetActive(false);
        ImageButton[0].enabled = false;
        ImageButton[1].enabled = true;
       if(SceneManager.GetActiveScene().name=="FreeMode") ImageButton[2].enabled = false;
        if (!isProcessing)
        {
            StartCoroutine(ShareScreenshot());
        }
    }
    public IEnumerator ShareScreenshot()
    {
        isProcessing = true;
        // wait for graphics to render
        yield return new WaitForEndOfFrame();
        //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- PHOTO
        // create the texture
        Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);
        // put buffer into texture
        screenTexture.ReadPixels(new Rect(0f, 0f, Screen.width, Screen.height), 0, 0);
        // apply
        screenTexture.Apply();
        //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- PHOTO
        byte[] dataToSave = screenTexture.EncodeToPNG();
        string destination = Path.Combine(Application.persistentDataPath, System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".png");
        File.WriteAllBytes(destination, dataToSave);
        if (!Application.isEditor)
        {
            // block to open the file and share it ------------START
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
            AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + destination);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);

            intentObject.Call<AndroidJavaObject>("setType", "text/plain");
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "" + Message);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), "SUBJECT");

            intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

            currentActivity.Call("startActivity", intentObject);
        }
        isProcessing = false;
        for (int i = 0; i < 2; i++) HideObj[i].SetActive(true);
        ImageButton[0].enabled = true;
        ImageButton[1].enabled = false;
        if (SceneManager.GetActiveScene().name == "FreeMode") ImageButton[2].enabled = true;
    }
}
