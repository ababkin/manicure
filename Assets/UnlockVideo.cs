﻿using UnityEngine;
using System.Collections;
using AppodealAds.Unity.Api;

public class UnlockVideo : MonoBehaviour {

    public void PlayAVideo()
    {
        //FindObjectOfType<AdColonyCustom>().PlayAVideo();
        AppodealController.Instance.ShowAd(Appodeal.NON_SKIPPABLE_VIDEO);
        gameObject.GetComponent<UnityEngine.UI.Image>().enabled = false;
    }
}
