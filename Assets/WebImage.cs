﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WebImage : MonoBehaviour {

    public string IconFS, Icon1, Icon2, Icon3, Icon4, Icon5;
    public string UrlBanner, Url1, Url2, Url3, Url4, Url5;
    public Image[] sprite;

    void Start() {     
        StartCoroutine(Banner());
        StartCoroutine(Image1());
        StartCoroutine(Image2());
        StartCoroutine(Image3());
        StartCoroutine(Image4());
        StartCoroutine(Image5());
    }

    IEnumerator Banner() {
        WWW wwwFullScreen = new WWW(IconFS);
        yield return wwwFullScreen;
        Sprite ImageFS = Sprite.Create(wwwFullScreen.texture, new Rect(0, 0, 1024, 500), new Vector2(0.1f, 0.1f));
        sprite[0].sprite = ImageFS;
    }

    IEnumerator Image1() {
        WWW wwwIcon1 = new WWW(Icon1);
        yield return wwwIcon1;
        Sprite ImageIc1 = Sprite.Create(wwwIcon1.texture, new Rect(0, 0, 512, 512), new Vector2(0.1f, 0.1f));
        sprite[1].sprite = ImageIc1;
    }
    IEnumerator Image2()
    {
        WWW wwwIcon2 = new WWW(Icon2);
        yield return wwwIcon2;
        Sprite ImageIc2 = Sprite.Create(wwwIcon2.texture, new Rect(0, 0, 512, 512), new Vector2(0.1f, 0.1f));
        sprite[2].sprite = ImageIc2;
    }
    IEnumerator Image3()
    {
        WWW wwwIcon3 = new WWW(Icon3);
        yield return wwwIcon3;
        Sprite ImageIc3 = Sprite.Create(wwwIcon3.texture, new Rect(0, 0, 512, 512), new Vector2(0.1f, 0.1f));
        sprite[3].sprite = ImageIc3;
    }
    IEnumerator Image4()
    {
        WWW wwwIcon4 = new WWW(Icon4);
        yield return wwwIcon4;
        Sprite ImageIc4 = Sprite.Create(wwwIcon4.texture, new Rect(0, 0, 512, 512), new Vector2(0.1f, 0.1f));
        sprite[4].sprite = ImageIc4;
    }
    IEnumerator Image5()
    {
        WWW wwwIcon5 = new WWW(Icon5);
        yield return wwwIcon5;
        Sprite ImageIc5 = Sprite.Create(wwwIcon5.texture, new Rect(0, 0, 512, 512), new Vector2(0.1f, 0.1f));
        sprite[5].sprite = ImageIc5;
    }

    public void OpenUrl(int NameButton) {
        switch (NameButton) {
            case 0:
                Application.OpenURL(UrlBanner);
                break;
            case 1:
                Application.OpenURL(Url1);
                break;
            case 2:
                Application.OpenURL(Url2);
                break;
            case 3:
                Application.OpenURL(Url3);
                break;
            case 4:
                Application.OpenURL(Url4);
                break;
            case 5:
                Application.OpenURL(Url5);
                break;
        }
    }
}
